# etech

# Description
A project web 1: website sales

# Using
- RectJS
- Atn Design

# Source
- BE: https://github.com/VBao/eShop_Php

# Preview Client
![image](/image/page-index.png)
![image](/image/page-index2.png)
![image](/image/page-filter.png)
![image](/image/page-productdetail.png)
![image](/image/page-job.png)
![image](/image/page-aboutus.png)
# Preview Admin
![image](/image/admin-list.png)
![image](/image/admin-add.png)
![image](/image/admin-bill.png)


