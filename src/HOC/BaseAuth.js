/* eslint-disable react-hooks/exhaustive-deps */
import { PATH } from "Constants/Path"
import { useEffect } from "react"
import { useDispatch } from "react-redux"
import { useHistory } from "react-router-dom"
import { getAuthCookie } from "Redux/User/User.thunk"

const Authentication = (SpecificComponent, option, adminRoute = null) => {
    function AuthenticationCheck(props) {
        const dispatch = useDispatch()
        const history = useHistory()

        const fetchAuth = async () => {
            const res = await dispatch(getAuthCookie())

            if (res && !res.isLogin) {
                //true => Login
                if (option) {
                    history.push(PATH.LOGIN)
                }
            } else {
                if (adminRoute && res && !res.isAdmin) {
                    //Not Admin
                    history.push(PATH.HOME)
                } else {
                    if (option === false) {
                        history.push(PATH.HOME)
                    }
                }
            }
        }
        useEffect(() => {
            fetchAuth()
            //To know my current status, send Auth request
        }, [])
        // Login Success
        return <SpecificComponent {...props} />
    }
    return AuthenticationCheck
}
export default Authentication
