import Cookies from "js-cookie"
import UserApi from "./User.Api"
import { getAuth, loginSuccess, logoutSuccess } from "./User.reducer"
export const login = ({ email, password, isRemember }) => async dispatch => {
    try {
        const resLogin = await UserApi.login({ email, password })

        if (resLogin.success) {
            Cookies.set("token", resLogin.data.token)
            Cookies.set("account", resLogin.data.info)
            Cookies.set("admin", resLogin.data.info.admin)
            if (isRemember) {
                Cookies.set("email", email)
                Cookies.set("password", password)
            } else {
                Cookies.set("email", "")
                Cookies.set("password", "")
            }
            dispatch(loginSuccess(resLogin.data.info))
            return true
        }
        return false
    } catch (e) {
        return false
    }
}

export const signup = body => async dispatch => {
    try {
        const res = await UserApi.signup(body)
        return res
    } catch (e) {
        return false
    }
}

export const forgotPassword = email => async dispatch => {
    try {
        const res = await UserApi.forgotPassword(email)

        return res
    } catch (e) {
        return false
    }
}

export const resetPassword = body => async dispatch => {
    try {
        const res = await UserApi.resetPassword(body)

        return res
    } catch (e) {
        return false
    }
}

export const updateInfo = body => async dispatch => {
    try {
        const res = await UserApi.updateInfo(body)

        return res
    } catch (e) {
        return false
    }
}

export const getAuthApi = () => async dispatch => {
    try {
        // const res={
        //   infor:{name:"My name is Admin"
        //   password:"My name is Admin"
        //   email:"admin@etech.com"
        //   phone:"0926753559"
        //   address:"uMeCtvDMc4zMVEZDWSEJJ2gjNzINK4"
        //   admin:1}
        // }
        // const res = await api.post('/api/auth/login/', { username, password })
        const resAuth = await UserApi.getAuth()
        if (resAuth) {
            dispatch(getAuth())
            return {
                isLogin: true,
                isAdmin: resAuth.info.isAdmin
            }
        }
        return {
            isLogin: false,
            isAdmin: null
        }
    } catch (e) {
        return {
            isLogin: false,
            isAdmin: null
        }
    }
}

export const getAuthCookie = () => async dispatch => {
    try {
        const data = Cookies.getJSON("account")

        if (data) {
            return {
                isLogin: true,
                isAdmin: data.admin === 1
            }
        }
        return {
            isLogin: false,
            isAdmin: null
        }
    } catch (e) {
        return {
            isLogin: false,
            isAdmin: null
        }
    }
}

export const logoutUser = () => async dispatch => {
    try {
        // const res = await api.post('/api/auth/logout/')
        Cookies.remove("token")
        return dispatch(logoutSuccess())
    } catch (e) {
        return console.log(e.message)
    }
}

export const getUserCookie = () => async dispatch => {
    const data = Cookies.getJSON("account")
    if (data) {
        dispatch(loginSuccess(data))
    }
}
