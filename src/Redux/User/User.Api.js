import axiosClient from "../../API/ClientAxios"

const url = "api/"

const login = async body => {
    try {
        const resApi = await axiosClient.post(`${url}login`, body)

        if (resApi?.info)
            return {
                success: true,
                data: resApi
            }
        return {
            success: false,
            data: {}
        }
    } catch (err) {
        console.log(err)
        return {
            success: false,
            data: {}
        }
    }
}

const signup = async body => {
    try {
        const resApi = await axiosClient.post(`${url}register`, body)
        if (resApi && resApi.success)
            return {
                success: true,
                data: resApi
            }
        return {
            success: false,
            data: {}
        }
    } catch (err) {
        console.log(err)
        return {
            success: false,
            data: {}
        }
    }
}

const forgotPassword = async email => {
    try {
        const resApi = await axiosClient.post(
            `${url}forget-password?email=${email}`
        )
        if (resApi && resApi?.result)
            return {
                success: true,
                data: resApi
            }
        return {
            success: false,
            data: {}
        }
    } catch (err) {
        console.log(err)
        return {
            success: false,
            data: {}
        }
    }
}

const resetPassword = async body => {
    try {
        const resApi = await axiosClient.post(`${url}reset-password`, body)
        if (resApi && resApi?.result)
            return {
                success: true,
                data: resApi
            }
        return {
            success: false,
            data: {}
        }
    } catch (err) {
        console.log(err)
        return {
            success: false,
            data: {}
        }
    }
}

const updateInfo = async body => {
    try {
        const resApi = await axiosClient.post(`${url}account/update_info`, body)
        if (resApi && resApi?.result)
            return {
                success: true,
                data: resApi
            }
        return {
            success: false,
            data: {}
        }
    } catch (err) {
        console.log(err)
        return {
            success: false,
            data: {}
        }
    }
}

const getAuth = async () => {
    try {
        const resApi = await axiosClient.post(`${url}getAuth`)
        if (resApi?.info)
            return {
                success: true,
                data: resApi
            }
        return {
            success: false,
            data: {}
        }
    } catch (err) {
        console.log(err)
        return {
            success: false,
            data: {}
        }
    }
}

const UserApi = {
    login,
    signup,
    forgotPassword,
    resetPassword,
    updateInfo
}
export default UserApi
