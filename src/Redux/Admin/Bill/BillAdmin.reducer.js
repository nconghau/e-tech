import { createSlice } from "@reduxjs/toolkit"

const slice = createSlice({
    name: "bill_admin",

    initialState: {
        bills: [],
        billsFilter: [],
        max_page: 0,
        curr_page: 0
    },

    reducers: {
        billsSuccess: (state, action) => {
            state.bills = action.payload
            state.billsFilter = action.payload
        },
        billsFail: (state, action) => {
            state.bills = null
            state.billsFilter = null
        },
        setMaxPage: (state, action) => {
            state.max_page = action.payload
        },
        setCurrPage: (state, action) => {
            state.curr_page = action.payload
        },
        handleSearchByName: (state, action) => {
            state.billsFilter = state.bills.filter(item =>
                item.name.toLowerCase().includes(action.payload.toLowerCase())
            )
        }
    }
})

export default slice.reducer

// Actions

export const {
    billsSuccess,
    billsFail,
    handleSearchByName,
    setMaxPage,
    setCurrPage
} = slice.actions
