import axiosClient from "API/ClientAxios"

const url = "api/admin/"

const getUsers = async page => {
    try {
        const resApi = await axiosClient.get(`${url}account/user?page=${page}`)
        if (resApi?.data)
            return {
                success: true,
                data: resApi.data
            }
        return {
            success: false,
            data: {}
        }
    } catch (err) {
        console.log(err)
        return {
            success: false,
            data: {}
        }
    }
}

const UserApi = {
    getUsers
}
export default UserApi
