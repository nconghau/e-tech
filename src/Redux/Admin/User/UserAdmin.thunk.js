import { changeLoading } from "Redux/System/System.reducer"
import UserApi from "./UserAdmin.Api"
import { setCurrPage, setMaxPage, setUsers } from "./UserAdmin.reducer"

export const getUsers = page => async dispatch => {
    try {
        dispatch(changeLoading(true))
        const resApi = await UserApi.getUsers(page)
        if (resApi.success) {
            dispatch(setUsers(resApi.data.data))
            dispatch(setCurrPage(resApi.data.curr_page))
            dispatch(setMaxPage(resApi.data.max_page))
        } else {
            dispatch(setUsers([]))
        }
        dispatch(changeLoading(false))
    } catch (err) {
        console.log(err)
        dispatch(changeLoading(false))
    }
}
