import { createSlice } from "@reduxjs/toolkit"
const slice = createSlice({
    name: "user_admin",

    initialState: {
        users: [],
        usersFilter: [],
        max_page: 0,
        curr_page: 0
    },

    reducers: {
        setUsers: (state, action) => {
            state.users = action.payload
            state.usersFilter = action.payload
        },
        setMaxPage: (state, action) => {
            state.max_page = action.payload
        },
        setCurrPage: (state, action) => {
            state.curr_page = action.payload
        },
        handleSearchByName: (state, action) => {
            state.usersFilter = state.users.filter(item =>
                item?.name.toLowerCase().includes(action.payload.toLowerCase())
            )
        }
    }
})

export default slice.reducer

// Actions

export const {
    setUsers,
    handleSearchByName,
    setMaxPage,
    setCurrPage
} = slice.actions
