import { changeLoading } from "Redux/System/System.reducer"
import StatisticApi from "./Statistic.Api"
import { setData, setNumber } from "./Statistic.reducer"

export const getStatistic = () => async dispatch => {
    try {
        dispatch(changeLoading(true))
        const resApi = await StatisticApi.getStatistic()
        if (resApi.success) {
            const number = {
                totalUser: resApi.data?.totalUser,
                newUserInMonth: resApi.data?.newUserInMonth,
                totalOrder: resApi.data?.totalOrder,
                newOrderInMonth: resApi.data?.newOrderInMonth
            }
            dispatch(setData(resApi.data?.chart))
            dispatch(setNumber(number))
        }
        dispatch(changeLoading(false))
    } catch (err) {
        console.log(err)
        dispatch(changeLoading(false))
    }
}
