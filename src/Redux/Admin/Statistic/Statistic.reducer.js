import { createSlice } from "@reduxjs/toolkit"
const slice = createSlice({
    name: "statistic_admin",

    initialState: {
        number: {},
        data: []
    },

    reducers: {
        setNumber: (state, action) => {
            state.number = action.payload
        },
        setData: (state, action) => {
            state.data = action.payload
        }
    }
})

export default slice.reducer

// Actions

export const { setNumber, setData } = slice.actions
