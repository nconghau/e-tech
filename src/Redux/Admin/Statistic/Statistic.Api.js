import axiosClient from "API/ClientAxios"

const url = "api/admin/"

const getStatistic = async () => {
    try {
        const resApi = await axiosClient.get(`${url}summary`)
        if (resApi)
            return {
                success: true,
                data: resApi
            }
        return {
            success: false,
            data: {}
        }
    } catch (err) {
        console.log(err)
        return {
            success: false,
            data: {}
        }
    }
}

const StatisticApi = {
    getStatistic
}
export default StatisticApi
