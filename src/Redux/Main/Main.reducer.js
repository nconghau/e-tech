const { createSlice } = require("@reduxjs/toolkit")

const slice = createSlice({
    name: "main",
    initialState: {
        toggle: false
    },
    reducers: {
        setToggle1: (state, action) => {
            state.toggle = action.payload
        }
    }
})

export default slice.reducer
export const { setToggle1 } = slice.actions
