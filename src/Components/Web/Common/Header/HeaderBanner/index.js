import { Carousel } from "antd"
import Images from "Constants/Images"

const HeaderBanner = () => {
    return (
        <div id="home">
            <Carousel autoplay effect={"scrollx"} dotPosition={"top"}>
                {/* <div className="banner banner-1">
                    <img alt="" src={Images.Banner_Levono_2}></img>
                </div>
                <div className="banner banner-1">
                    <img alt="" src={Images.Banner_HP_Envy}></img>
                </div>
                <div className="banner banner-1">
                    <img alt="" src={Images.Banner_Levono}></img>
                </div>
                <div className="banner banner-1">
                    <img alt="" src={Images.Banner_HP}></img>
                </div> */}
                <div className="banner banner-1">
                    <img alt="" src={Images.Banner1}></img>
                </div>
                <div className="banner banner-1">
                    <img alt="" src={Images.Banner2}></img>
                </div>
                <div className="banner banner-1">
                    <img alt="" src={Images.Banner3}></img>
                </div>
            </Carousel>
        </div>
    )
}
export default HeaderBanner
