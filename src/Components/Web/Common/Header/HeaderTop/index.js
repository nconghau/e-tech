import {
    BarChartOutlined,
    HeartOutlined,
    HistoryOutlined,
    PhoneOutlined,
    PoweroffOutlined,
    RightCircleOutlined,
    ShoppingCartOutlined,
    UserAddOutlined,
    UserOutlined
} from "@ant-design/icons"
import { notification } from "antd"
import {
    NAV_MAP_DRIVE,
    NAV_MAP_KEYBOARD,
    NAV_MAP_LAPTOP_ENGINEERING,
    NAV_MAP_LAPTOP_GAMING,
    NAV_MAP_LAPTOP_OFFICE,
    NAV_MAP_MOUSE
} from "Constants/Data"
import Images from "Constants/Images"
import { PATH } from "Constants/Path"
import React, { useEffect, useState } from "react"
import { useDispatch, useSelector } from "react-redux"
import { useHistory } from "react-router-dom"
import { Link } from "react-router-dom"
import {
    getTotalPriceBillSuccess,
    getTotalPriceSuccess
} from "Redux/Cart/Cart.reducer"
import { getCartLS, setCartLS } from "Redux/Cart/Cart.thunk"
import { getUserCookie, logoutUser } from "Redux/User/User.thunk"
import { changePriceToVND } from "Utils/Converter"

const HeaderTop = () => {
    const dispatch = useDispatch()
    const account = useSelector(state => state.UserReducer.user)

    const [toggleCart, setToggleCart] = useState(false)
    const [toggleDropDown, setToggleDropDown] = useState(false)
    const carts = useSelector(state => state.CartReducer.carts)
    const admin = useSelector(state => state.UserReducer.admin)
    const totalPrice = useSelector(state => state.CartReducer.totalPrice)
    const countItem = carts?.length

    useEffect(() => {
        dispatch(getUserCookie())

        dispatch(getCartLS())
        dispatch(getTotalPriceBillSuccess())
        dispatch(getTotalPriceSuccess())
    }, [])

    const handleRemoveItem = id => {
        dispatch(setCartLS(carts.filter(item => item.id !== id)))
        dispatch(getTotalPriceSuccess())
    }

    const logout = () => {
        dispatch(logoutUser())
    }

    const [toggle, setToggle] = useState(false)
    const [openRow1, setOpenRow1] = useState(false)
    const [openRow2, setOpenRow2] = useState(false)
    const [openRow3, setOpenRow3] = useState(false)
    const [openRow4, setOpenRow4] = useState(false)
    const [openRow5, setOpenRow5] = useState(false)
    const [openRow6, setOpenRow6] = useState(false)
    // const isHomePage = String(window.location.pathname) === PATH.HOME

    const [toggleOpenNav, setToggleOpenNav] = useState(false)
    const [toggleOpenMenu, setToggleOpenMenu] = useState(false)
    const [toggleOpenCategory, setToggleOpenCategory] = useState(false)
    React.useEffect(() => {
        document.body.addEventListener(
            "click",
            function (e) {
                if (e.target.className === "shadow") {
                    setToggleOpenNav(false)
                }
            },
            false
        )
    }, [toggleOpenNav])

    const history = useHistory()
    const [keyword, setKeyword] = useState("")
    const handleSearch = () => {
        setKeyword("")
        history.push(`/etech/search/filter/keyword=${keyword}`)
    }

    return (
        <>
            {/* {account?.name ? (
                <div id="top-header">
                    <div className="container">
                        <div className="pull-left">
                            <span>Hello, {account?.name || "Bạn"}</span>
                        </div>
                    </div>
                </div>
            ) : (
                ""
            )} */}

            <header>
                <div id="header">
                    <div className="container">
                        <div className="pull-left">
                            <div className="header-logo">
                                <Link className="logo" to={PATH.HOME}>
                                    <img alt="" src={Images.Logo}></img>
                                </Link>
                            </div>

                            <div className="header-search">
                                <div className="form-search">
                                    <input
                                        className="input search-input"
                                        type="text"
                                        placeholder="Tìm sản phẩm..."
                                        value={keyword}
                                        onChange={e =>
                                            setKeyword(e.target.value)
                                        }
                                        onKeyDown={e => {
                                            if (e.key === "Enter") {
                                                handleSearch()
                                            }
                                        }}
                                    />

                                    <button
                                        className="search-btn"
                                        onClick={() => {
                                            handleSearch()
                                        }}
                                    >
                                        <i className="fa fa-search"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div className="pull-right">
                            <ul className="header-btns">
                                <li
                                    onMouseEnter={() => {
                                        setToggleDropDown(true)
                                    }}
                                    onMouseLeave={() => {
                                        setToggleDropDown(false)
                                    }}
                                    className={
                                        toggleDropDown
                                            ? "header-account dropdown default-dropdown open"
                                            : "header-account dropdown default-dropdown"
                                    }
                                >
                                    <div
                                        className="dropdown-toggle"
                                        role="button"
                                        data-toggle="dropdown"
                                        aria-expanded={
                                            toggle ? "false" : "true"
                                        }
                                    >
                                        <div className="header-btns-icon">
                                            <UserOutlined />
                                        </div>
                                        <strong className="text-uppercase">
                                            Tài Khoản
                                            {/* &emsp;<i className="fa fa-caret-down"></i> */}
                                        </strong>
                                    </div>
                                    <Link
                                        to={PATH.LOGIN}
                                        style={{
                                            display:
                                                account === null
                                                    ? "block"
                                                    : "none"
                                        }}
                                    >
                                        Đăng nhập
                                    </Link>
                                    <ul className="custom-menu">
                                        <li
                                            style={{
                                                display:
                                                    admin && admin === 1
                                                        ? "block"
                                                        : "none"
                                            }}
                                        >
                                            <Link to="/admin/home">
                                                <BarChartOutlined
                                                    style={{
                                                        marginRight: "15px",
                                                        color:
                                                            "var(--color-primary)"
                                                    }}
                                                />
                                                Trang quản lý
                                            </Link>
                                        </li>
                                        <li>
                                            <Link
                                                to="/etech/yeu-thich"
                                                className="disabled-link"
                                                onClick={() =>
                                                    notification.warning({
                                                        message: "Thông báo",
                                                        description:
                                                            "Tính năng đang cập nhật"
                                                    })
                                                }
                                            >
                                                <HeartOutlined
                                                    style={{
                                                        marginRight: "15px",
                                                        color:
                                                            "var(--color-primary)"
                                                    }}
                                                />
                                                Yêu thích
                                            </Link>
                                        </li>
                                        <li
                                            style={{
                                                display:
                                                    account !== null
                                                        ? "none"
                                                        : "block"
                                            }}
                                        >
                                            <Link to={PATH.SIGNUP}>
                                                <UserAddOutlined
                                                    style={{
                                                        marginRight: "15px",
                                                        color:
                                                            "var(--color-primary)"
                                                    }}
                                                />
                                                Tạo tài khoản
                                            </Link>
                                        </li>
                                        <li
                                            style={{
                                                display:
                                                    account === null
                                                        ? "none"
                                                        : "block"
                                            }}
                                        >
                                            <Link to={`${PATH.HOME}/lich-su`}>
                                                <HistoryOutlined
                                                    style={{
                                                        marginRight: "15px",
                                                        color:
                                                            "var(--color-primary)"
                                                    }}
                                                />
                                                Lịch sử mua hàng
                                            </Link>
                                        </li>
                                        <li
                                            style={{
                                                display:
                                                    account === null
                                                        ? "none"
                                                        : "block"
                                            }}
                                        >
                                            <Link to={`${PATH.HOME}/tai-khoan`}>
                                                <UserOutlined
                                                    style={{
                                                        marginRight: "15px",
                                                        color:
                                                            "var(--color-primary)"
                                                    }}
                                                />
                                                Tài khoản của tôi
                                            </Link>
                                        </li>
                                        <li
                                            style={{
                                                display:
                                                    account === null
                                                        ? "none"
                                                        : "block"
                                            }}
                                        >
                                            <Link
                                                to={PATH.HOME}
                                                onClick={() => logout()}
                                            >
                                                <PoweroffOutlined
                                                    style={{
                                                        marginRight: "15px",
                                                        color:
                                                            "var(--color-primary)"
                                                    }}
                                                />
                                                Đăng xuất
                                            </Link>
                                        </li>
                                    </ul>
                                </li>

                                <li
                                    onMouseEnter={() => setToggleCart(true)}
                                    onMouseLeave={() => setToggleCart(false)}
                                    className={
                                        toggleCart
                                            ? "header-cart dropdown default-dropdown open"
                                            : "header-cart dropdown default-dropdown"
                                    }
                                >
                                    <div
                                        className="dropdown-toggle"
                                        data-toggle="dropdown"
                                        aria-expanded={
                                            toggleCart ? "true" : "false"
                                        }
                                        style={{ width: "170px" }}
                                    >
                                        <div className="header-btns-icon">
                                            <ShoppingCartOutlined />
                                            <span className="qty">
                                                {countItem}
                                            </span>
                                        </div>
                                        <strong className="text-uppercase">
                                            Giỏ Hàng
                                        </strong>
                                        <br></br>
                                        <span>
                                            {changePriceToVND(totalPrice)}
                                        </span>
                                    </div>
                                    {/* <CartComponent/> */}
                                    <div className="custom-menu">
                                        <div id="shopping-cart">
                                            {carts?.map(item => {
                                                return (
                                                    <div
                                                        className="shopping-cart-list"
                                                        key={item.id}
                                                    >
                                                        <div className="product product-widget">
                                                            <div className="product-thumb">
                                                                <img
                                                                    alt=""
                                                                    src={
                                                                        item.image
                                                                    }
                                                                ></img>
                                                            </div>
                                                            <div className="product-body">
                                                                <h2 className="product-name">
                                                                    <Link
                                                                        to={`${PATH.LAPTOP}/${item.id}`}
                                                                    >
                                                                        {
                                                                            item.name
                                                                        }
                                                                    </Link>
                                                                </h2>
                                                                <p className="product-price">
                                                                    {changePriceToVND(
                                                                        item.price
                                                                    )}
                                                                    {/* <span className="qty">x1</span> */}
                                                                </p>
                                                            </div>
                                                            <button
                                                                className="cancel-btn"
                                                                onClick={() =>
                                                                    handleRemoveItem(
                                                                        item.id
                                                                    )
                                                                }
                                                            >
                                                                <i className="fa fa-trash"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                )
                                            })}
                                            {/* Button => check out */}
                                            <div className="shopping-cart-btns">
                                                <Link
                                                    to={PATH.CHECK_OUT}
                                                    className="primary-btn"
                                                >
                                                    Xem giỏ hàng &thinsp;
                                                    <RightCircleOutlined />
                                                </Link>
                                            </div>
                                        </div>
                                    </div>
                                </li>

                                <li className="nav-toggle">
                                    <button
                                        className="nav-toggle-btn main-btn icon-btn"
                                        onClick={() => {
                                            setToggleOpenNav(true)
                                        }}
                                    >
                                        <i className="fa fa-bars"></i>
                                    </button>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </header>

            <div id="navigation" className={toggleOpenNav ? "shadow" : ""}>
                <div className="container">
                    <div
                        id="responsive-nav"
                        className={toggleOpenNav ? "open" : ""}
                    >
                        <div
                            className="category-nav show-on-click"
                            onMouseEnter={() => setToggle(true)}
                            onMouseLeave={() => setToggle(false)}
                        >
                            <span className="category-header">
                                Danh Mục Sản Phẩm
                                <i
                                    className="fa fa-list"
                                    onClick={() =>
                                        setToggleOpenCategory(
                                            !toggleOpenCategory
                                        )
                                    }
                                ></i>
                            </span>
                            <ul
                                className={
                                    toggleOpenCategory
                                        ? "category-list open"
                                        : "category-list"
                                }
                            >
                                <li
                                    onMouseEnter={() => setOpenRow1(!openRow1)}
                                    onMouseLeave={() => setOpenRow1(!openRow1)}
                                    className={
                                        openRow1
                                            ? "dropdown side-dropdown open"
                                            : "dropdown side-dropdown"
                                    }
                                >
                                    <p
                                        className="dropdown-toggle"
                                        data-toggle="dropdown"
                                        aria-expanded="true"
                                    >
                                        Laptop Gamming
                                        <i className="fa fa-angle-right"></i>
                                    </p>
                                    <div className="custom-menu">
                                        <div className="row">
                                            {NAV_MAP_LAPTOP_GAMING?.map(
                                                item => {
                                                    return (
                                                        <div
                                                            className="col-md-4"
                                                            key={item.id}
                                                        >
                                                            <ul className="list-links">
                                                                <li>
                                                                    <h3 className="list-links-title">
                                                                        {
                                                                            item.title
                                                                        }
                                                                    </h3>
                                                                </li>
                                                                {item.nav_map_li &&
                                                                    item.nav_map_li.map(
                                                                        item => {
                                                                            return (
                                                                                <li
                                                                                    key={
                                                                                        item.id
                                                                                    }
                                                                                >
                                                                                    <a
                                                                                        href={`${PATH.HOME}/laptop/filter/${item.link}`}
                                                                                    >
                                                                                        {
                                                                                            item.title
                                                                                        }
                                                                                    </a>
                                                                                </li>
                                                                            )
                                                                        }
                                                                    )}
                                                            </ul>
                                                            <hr className="hidden-md hidden-lg"></hr>
                                                        </div>
                                                    )
                                                }
                                            )}
                                        </div>
                                    </div>
                                </li>
                                <li
                                    onMouseEnter={() => setOpenRow2(!openRow2)}
                                    onMouseLeave={() => setOpenRow2(!openRow2)}
                                    className={
                                        openRow2
                                            ? "dropdown side-dropdown open"
                                            : "dropdown side-dropdown"
                                    }
                                >
                                    <p
                                        className="dropdown-toggle"
                                        data-toggle="dropdown"
                                        aria-expanded="true"
                                    >
                                        Laptop Học Tập & Văn Phòng
                                        <i className="fa fa-angle-right"></i>
                                    </p>
                                    <div className="custom-menu">
                                        <div className="row">
                                            {NAV_MAP_LAPTOP_OFFICE?.map(
                                                item => {
                                                    return (
                                                        <div
                                                            className="col-md-4"
                                                            key={item.id}
                                                        >
                                                            <ul className="list-links">
                                                                <li>
                                                                    <h3 className="list-links-title">
                                                                        {
                                                                            item.title
                                                                        }
                                                                    </h3>
                                                                </li>
                                                                {item.nav_map_li &&
                                                                    item.nav_map_li.map(
                                                                        item => {
                                                                            return (
                                                                                <li
                                                                                    key={
                                                                                        item.id
                                                                                    }
                                                                                >
                                                                                    <a
                                                                                        href={`${PATH.HOME}/laptop/filter/${item.link}`}
                                                                                    >
                                                                                        {
                                                                                            item.title
                                                                                        }
                                                                                    </a>
                                                                                </li>
                                                                            )
                                                                        }
                                                                    )}
                                                            </ul>
                                                            <hr className="hidden-md hidden-lg"></hr>
                                                        </div>
                                                    )
                                                }
                                            )}
                                        </div>
                                    </div>
                                </li>
                                <li
                                    onMouseEnter={() => setOpenRow3(!openRow3)}
                                    onMouseLeave={() => setOpenRow3(!openRow3)}
                                    className={
                                        openRow3
                                            ? "dropdown side-dropdown open"
                                            : "dropdown side-dropdown"
                                    }
                                >
                                    <p
                                        className="dropdown-toggle"
                                        data-toggle="dropdown"
                                        aria-expanded="true"
                                    >
                                        Laptop Đồ Họa & Kỹ Thuật
                                        <i className="fa fa-angle-right"></i>
                                    </p>
                                    <div className="custom-menu">
                                        <div className="row">
                                            {NAV_MAP_LAPTOP_ENGINEERING?.map(
                                                item => {
                                                    return (
                                                        <div
                                                            className="col-md-4"
                                                            key={item.id}
                                                        >
                                                            <ul className="list-links">
                                                                <li>
                                                                    <h3 className="list-links-title">
                                                                        {
                                                                            item.title
                                                                        }
                                                                    </h3>
                                                                </li>
                                                                {item.nav_map_li &&
                                                                    item.nav_map_li.map(
                                                                        item => {
                                                                            return (
                                                                                <li
                                                                                    key={
                                                                                        item.id
                                                                                    }
                                                                                >
                                                                                    <a
                                                                                        href={`${PATH.HOME}/laptop/filter/${item.link}`}
                                                                                    >
                                                                                        {
                                                                                            item.title
                                                                                        }
                                                                                    </a>
                                                                                </li>
                                                                            )
                                                                        }
                                                                    )}
                                                            </ul>
                                                            <hr className="hidden-md hidden-lg"></hr>
                                                        </div>
                                                    )
                                                }
                                            )}
                                        </div>
                                    </div>
                                </li>
                                <li
                                    onMouseEnter={() => setOpenRow4(!openRow4)}
                                    onMouseLeave={() => setOpenRow4(!openRow4)}
                                    className={
                                        openRow4
                                            ? "dropdown side-dropdown open"
                                            : "dropdown side-dropdown"
                                    }
                                >
                                    <p
                                        className="dropdown-toggle"
                                        data-toggle="dropdown"
                                        aria-expanded="true"
                                    >
                                        Ổ cứng
                                        <i className="fa fa-angle-right"></i>
                                    </p>
                                    <div className="custom-menu">
                                        <div className="row">
                                            {NAV_MAP_DRIVE?.map(item => {
                                                return (
                                                    <div
                                                        className="col-md-4"
                                                        key={item.id}
                                                    >
                                                        <ul className="list-links">
                                                            <li>
                                                                <h3 className="list-links-title">
                                                                    {item.title}
                                                                </h3>
                                                            </li>
                                                            {item.nav_map_li &&
                                                                item.nav_map_li.map(
                                                                    item => {
                                                                        return (
                                                                            <li
                                                                                key={
                                                                                    item.id
                                                                                }
                                                                            >
                                                                                <a
                                                                                    href={`${PATH.HOME}/drive/filter/${item.link}`}
                                                                                >
                                                                                    {
                                                                                        item.title
                                                                                    }
                                                                                </a>
                                                                            </li>
                                                                        )
                                                                    }
                                                                )}
                                                        </ul>
                                                        <hr className="hidden-md hidden-lg"></hr>
                                                    </div>
                                                )
                                            })}
                                        </div>
                                    </div>
                                </li>
                                <li
                                    onMouseEnter={() => setOpenRow5(!openRow5)}
                                    onMouseLeave={() => setOpenRow5(!openRow5)}
                                    className={
                                        openRow5
                                            ? "dropdown side-dropdown open"
                                            : "dropdown side-dropdown"
                                    }
                                >
                                    <p
                                        className="dropdown-toggle"
                                        data-toggle="dropdown"
                                        aria-expanded="true"
                                    >
                                        Chuột máy tính
                                        <i className="fa fa-angle-right"></i>
                                    </p>
                                    <div className="custom-menu">
                                        <div className="row">
                                            {NAV_MAP_MOUSE?.map(item => {
                                                return (
                                                    <div
                                                        className="col-md-4"
                                                        key={item.id}
                                                    >
                                                        <ul className="list-links">
                                                            <li>
                                                                <h3 className="list-links-title">
                                                                    {item.title}
                                                                </h3>
                                                            </li>
                                                            {item.nav_map_li &&
                                                                item.nav_map_li.map(
                                                                    item => {
                                                                        return (
                                                                            <li
                                                                                key={
                                                                                    item.id
                                                                                }
                                                                            >
                                                                                <a
                                                                                    href={`${PATH.HOME}/mouse/filter/${item.link}`}
                                                                                    style={{
                                                                                        pointerEvents:
                                                                                            "none"
                                                                                    }}
                                                                                >
                                                                                    {
                                                                                        item.title
                                                                                    }
                                                                                </a>
                                                                            </li>
                                                                        )
                                                                    }
                                                                )}
                                                        </ul>
                                                        <hr className="hidden-md hidden-lg"></hr>
                                                    </div>
                                                )
                                            })}
                                        </div>
                                    </div>
                                </li>
                                <li
                                    onMouseEnter={() => setOpenRow6(!openRow6)}
                                    onMouseLeave={() => setOpenRow6(!openRow6)}
                                    className={
                                        openRow6
                                            ? "dropdown side-dropdown open"
                                            : "dropdown side-dropdown"
                                    }
                                >
                                    <p
                                        className="dropdown-toggle"
                                        data-toggle="dropdown"
                                        aria-expanded="true"
                                    >
                                        Bàn phím
                                        <i className="fa fa-angle-right"></i>
                                    </p>
                                    <div className="custom-menu">
                                        <div className="row">
                                            {NAV_MAP_KEYBOARD?.map(item => {
                                                return (
                                                    <div
                                                        className="col-md-4"
                                                        key={item.id}
                                                    >
                                                        <ul className="list-links">
                                                            <li>
                                                                <h3 className="list-links-title">
                                                                    {item.title}
                                                                </h3>
                                                            </li>
                                                            {item?.nav_map_li?.map(
                                                                item => {
                                                                    return (
                                                                        <li
                                                                            key={
                                                                                item.id
                                                                            }
                                                                        >
                                                                            <a
                                                                                href={`${PATH.HOME}/keyboard /filter/${item.link}`}
                                                                                style={{
                                                                                    pointerEvents:
                                                                                        "none"
                                                                                }}
                                                                            >
                                                                                {
                                                                                    item.title
                                                                                }
                                                                            </a>
                                                                        </li>
                                                                    )
                                                                }
                                                            )}
                                                        </ul>
                                                        <hr className="hidden-md hidden-lg"></hr>
                                                    </div>
                                                )
                                            })}
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>

                        {/* menu nav */}
                        <div className="menu-nav">
                            <span className="menu-header">
                                Menu{" "}
                                <i
                                    className="fa fa-bars"
                                    onClick={() =>
                                        setToggleOpenMenu(!toggleOpenMenu)
                                    }
                                ></i>
                            </span>
                            <ul
                                className={
                                    toggleOpenMenu
                                        ? "menu-list open"
                                        : "menu-list"
                                }
                            >
                                <li>
                                    <Link to={PATH.HOME}>Trang Chủ</Link>
                                </li>

                                <li>
                                    <Link to={PATH.DISCOUNT}>Khuyến Mãi</Link>
                                </li>
                                <li>
                                    <Link to={PATH.ABOUT}>Giới Thiệu</Link>
                                </li>
                                <li>
                                    <Link
                                        to="/etech/hostline"
                                        className="disabled-link"
                                        style={{ color: "#33c9dc" }}
                                    >
                                        HOSTLINE &thinsp;
                                        <PhoneOutlined />
                                        0944445555
                                    </Link>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default HeaderTop
