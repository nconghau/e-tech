import { Image } from "antd"
import React from "react"
import "./ProductViewImage.css"

const ProductViewImage = props => {
    const { image } = props

    const [linkImg, setLinkImg] = React.useState("")

    React.useEffect(() => {
        setLinkImg(image[0].img)
    }, [image])

    const onShow = id => {
        let index = image.findIndex(x => x.id === id)
        setLinkImg(image[Number(index)].img)
        //const imageEntriesTest = Object.entries(imageTest)
    }

    return (
        <div className="col-md-6">
            <div id="product-main-view">
                <div className="product-view">
                    <div className="column">
                        <Image src={linkImg && linkImg} />
                    </div>

                    <div id="slide-wrapper">
                        <div id="slider">
                            {image?.map(item => {
                                return (
                                    <div
                                        key={item.id}
                                        onMouseOver={() => onShow(item.id)}
                                    >
                                        <img
                                            alt=""
                                            className={"thumbnail"}
                                            src={item.img}
                                        ></img>
                                    </div>
                                )
                            })}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ProductViewImage
