import { notification } from "antd"
import { useDispatch } from "react-redux"
import { addCart } from "Redux/Cart/Cart.reducer"
import { changePriceToVND } from "Utils/Converter"
import ProductSpec from "./ProductSpec"

const ProductDetail = props => {
    const { detail, discount, image, id } = props
    const dispatch = useDispatch()
    const saveProductToLocalStorage = () => {
        let cartItem = {
            id: id,
            image: image[0].img,
            name: detail.name,
            price:
                discount?.discount_price > 0
                    ? discount?.discount_price
                    : detail.price,
            spec1: detail.ram,
            spec2: detail.rom,
            qty: 1
        }
        dispatch(addCart(cartItem))
        notification.success({
            message: "Thông báo",
            description: "Thêm vào giỏ hàng thành công!"
        })
    }

    const renderPrice = () => {
        if (discount?.discount_price > 0 && discount?.discount_price > 0) {
            return (
                <h3 className="product-price">
                    {changePriceToVND(discount?.discount_price)} {""}
                    <del className="product-old-price">
                        {changePriceToVND(detail?.price)}
                    </del>
                </h3>
            )
        } else {
            return (
                <h3 className="product-price">
                    {changePriceToVND(detail?.price)}
                </h3>
            )
        }
    }

    return (
        <div className="col-md-6">
            <div className="product-body">
                {discount?.discount_percent > 0 && (
                    <div className="product-label">
                        <span>Khuyến mãi</span>
                        <span className="sale">
                            -{discount?.discount_percent}%
                        </span>
                    </div>
                )}
                <h2 className="product-name">{detail?.name}</h2>
                {renderPrice()}
                <div>
                    <div className="product-rating">
                        <i className="fa fa-star"></i>
                        <i className="fa fa-star"></i>
                        <i className="fa fa-star"></i>
                        <i className="fa fa-star"></i>
                        <i className="fa fa-star-o empty"></i>
                    </div>
                    <p>15 Đánh giá / Thêm đánh giá</p>
                </div>
                {/* change product type */}
                <ProductSpec detail={detail} />
                <div className="product-options">
                    {/* <ul className="color-option">
                        <li>
                            <span className="text-uppercase">Màu sắc:</span>
                        </li>
                        <li className="active">
                            <a href={"/#"} style={{ background: "#fff" }}></a>
                        </li>
                        <li>
                            <a href={"/#"} style={{ background: "#000" }}></a>
                        </li>
                    </ul> */}
                </div>

                <div className="product-btns">
                    <button
                        className="primary-btn add-to-cart"
                        disabled={id === 0 ? true : false}
                        onClick={() => saveProductToLocalStorage()}
                    >
                        <i className="fa fa-shopping-cart"></i> Thêm vào giỏ
                        hàng
                    </button>
                    <div className="pull-right">
                        <button className="main-btn icon-btn">
                            <i className="fa fa-heart"></i>
                        </button>
                        <button
                            className="main-btn icon-btn"
                            onClick={() => {
                                navigator.clipboard.writeText(
                                    window.location.href
                                )
                            }}
                        >
                            <i className="fa fa-share-alt"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default ProductDetail
