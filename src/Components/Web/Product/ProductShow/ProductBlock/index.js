import { notification, Tag } from "antd"
import { PATH } from "Constants/Path"
import { useDispatch } from "react-redux"
import { Link, useHistory } from "react-router-dom"
import { addCart } from "Redux/Cart/Cart.reducer"
import { changePriceToVND } from "Utils/Converter"

const ProductBlock = props => {
    const {
        type,
        id,
        image,
        name,
        price,
        spec1,
        spec2,
        discountPrice,
        discountPercent
    } = props

    const history = useHistory()
    const dispatch = useDispatch()
    const saveProductToLocalStorage = () => {
        let cartItem = {
            id: id,
            image: image,
            name: name,
            price: discountPrice > 0 ? discountPrice : price,
            spec1: spec1,
            spec2: spec2,
            qty: 1,
            type: type
        }
        dispatch(addCart(cartItem))
        notification.success({
            message: "Thông báo",
            description: "Thêm vào giỏ hàng thành công!"
        })
    }

    const renderPrice = () => {
        if (discountPrice > 0 && discountPercent > 0) {
            return (
                <>
                    <div className="product-price">
                        <s>{changePriceToVND(price)}</s>{" "}
                        <span>{`-${discountPercent}%`}</span>
                    </div>
                    <h4 className="product-price">
                        {changePriceToVND(discountPrice)}
                    </h4>
                </>
            )
        } else {
            return (
                <>
                    {" "}
                    <h4 className="product-price">
                        {changePriceToVND(price)}
                    </h4>{" "}
                </>
            )
        }
    }

    return (
        <div className="col-md-3 col-sm-6 col-xs-12">
            <div className="product product-single">
                <div
                    className="product-thumb"
                    onClick={() => history.push(`${PATH.HOME}/${type}/${id}`)}
                >
                    <button className="main-btn quick-view">
                        <Link to={`${PATH.HOME}/${type && type}/${id && id}`}>
                            <i className="fa fa-search-plus"></i> Xem Chi Tiết
                        </Link>
                    </button>
                    <img src={image} alt=""></img>
                </div>
                <div className="product-body">
                    <h2 className="product-name">
                        <Link
                            to={`${PATH.HOME}/${type}/${id}`}
                            className="product-name link"
                        >
                            {name?.length > 40
                                ? name.slice(0, 40) + "..."
                                : name}
                        </Link>
                    </h2>
                    {name?.length < 27 ? <br></br> : null}
                    <p>
                        <Tag>{spec1} </Tag>
                        <Tag>{spec2} </Tag>
                    </p>
                    {renderPrice()}
                    {/* <div className="product-rating">
                        <i className="fa fa-star"></i>
                        <i className="fa fa-star"></i>
                        <i className="fa fa-star"></i>
                        <i className="fa fa-star"></i>
                        <i className="fa fa-star-o empty"></i>
                        15
                    </div> */}
                    <div className="product-btns">
                        <button
                            className="primary-btn add-to-cart"
                            onClick={() => saveProductToLocalStorage()}
                        >
                            <i className="fa fa-shopping-cart"></i> Thêm vào giỏ
                            hàng
                        </button>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default ProductBlock
