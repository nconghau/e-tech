import React from "react"
const ProductBlock = React.lazy(() => import("../ProductBlock"))

const SelectBlock = props => {
    const { selectBlockTitle, products } = props

    return (
        <div className="section">
            <div className="container">
                <div className="row">
                    <div className="col-md-12">
                        <div className="section-title">
                            <h3 className="title">{selectBlockTitle}</h3>
                        </div>
                    </div>
                    <div
                        className="col-md-12"
                        style={{ display: "flex", flexWrap: "wrap" }}
                    >
                        {products?.map(item => {
                            return (
                                <ProductBlock
                                    key={item?.id}
                                    type={item?.type}
                                    id={item?.id}
                                    image={item?.image}
                                    name={item?.name}
                                    spec1={item?.spec1}
                                    spec2={item?.spec2}
                                    price={item?.price}
                                    discountPrice={item?.discount_price}
                                    discountPercent={item?.discount_percent}
                                    guarantee={item?.guarantee}
                                />
                            )
                        })}
                    </div>
                </div>
            </div>
        </div>
    )
}
export default SelectBlock
