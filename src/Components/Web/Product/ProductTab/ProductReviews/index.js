import { Button, notification, Rate } from "antd"
const ProductReviews = () => {
    return (
        <div id="tab2" className="tab-pane fade in active">
            <div className="row">
                <div className="col-md-6">
                    <div className="product-reviews">
                        <div className="single-review">
                            <div className="review-heading">
                                <div>Admin</div>
                                <div>
                                    <a href>
                                        <i className="fa fa-clock-o" />{" "}
                                        {new Date().toLocaleDateString("en-GB")}
                                    </a>
                                </div>
                                <div className="review-rating pull-right">
                                    <i className="fa fa-star" />
                                    <i className="fa fa-star" />
                                    <i className="fa fa-star" />
                                    <i className="fa fa-star" />
                                    <i className="fa fa-star-o empty" />
                                </div>
                            </div>
                            <div className="review-body">
                                <p>Chức năng đánh giá chưa được cập nhật!</p>
                            </div>
                        </div>
                        {/* <div className="single-review">
                            <div className="review-heading">
                                <div>
                                    <a href="/#">
                                        <i className="fa fa-user-o" /> John
                                    </a>
                                </div>
                                <div>
                                    <a href="/#">
                                        <i className="fa fa-clock-o" /> 27 DEC
                                        2017 / 8:0 PM
                                    </a>
                                </div>
                                <div className="review-rating pull-right">
                                    <i className="fa fa-star" />
                                    <i className="fa fa-star" />
                                    <i className="fa fa-star" />
                                    <i className="fa fa-star" />
                                    <i className="fa fa-star-o empty" />
                                </div>
                            </div>
                            <div className="review-body">
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur
                                    adipisicing elit, sed do eiusmod tempor
                                    incididunt ut labore et dolore magna
                                    aliqua.Ut enim ad minim veniam, quis nostrud
                                    exercitation ullamco laboris nisi ut aliquip
                                    ex ea commodo consequat.Duis aute irure
                                    dolor in reprehenderit in voluptate velit
                                    esse cillum dolore eu fugiat nulla pariatur.
                                </p>
                            </div>
                        </div>
                        <div className="single-review">
                            <div className="review-heading">
                                <div>
                                    <a href="/#">
                                        <i className="fa fa-user-o" /> John
                                    </a>
                                </div>
                                <div>
                                    <a href="/#">
                                        <i className="fa fa-clock-o" /> 27 DEC
                                        2017 / 8:0 PM
                                    </a>
                                </div>
                                <div className="review-rating pull-right">
                                    <i className="fa fa-star" />
                                    <i className="fa fa-star" />
                                    <i className="fa fa-star" />
                                    <i className="fa fa-star" />
                                    <i className="fa fa-star-o empty" />
                                </div>
                            </div>

                            <div className="review-body">
                                <p></p>
                            </div>
                        </div> */}

                        {/* <Pagination
                            style={{ textAlign: "center" }}
                            size="small"
                            total={50}
                        /> */}
                    </div>
                </div>

                <div className="col-md-6">
                    <h4 className="text-uppercase">
                        <b>Đánh giá của bạn</b>
                    </h4>
                    <form className="review-form">
                        <div className="form-group">
                            <textarea
                                className="input"
                                placeholder="Phần đánh giá sản phẩm"
                                defaultValue={""}
                            />
                        </div>

                        {/* <strong className="text-uppercase">: Đánh Giá</strong>
                        <div style={{ float: "left", marginTop: "-8px" }}>
                            <Rate defaultValue={0} />
                            <span className="ant-rate-text"></span>
                        </div>
                        <br />
                        <br /> */}

                        <button
                            type="button"
                            className="primary-btn add-to-cart"
                            onClick={() =>
                                notification.warning({
                                    message: "Thông báo",
                                    description: "Tính năng đang cập nhật"
                                })
                            }
                        >
                            <i className="fa fa-paper-plane"></i> Đánh giá
                        </button>
                    </form>
                </div>
            </div>
        </div>
    )
}
export default ProductReviews
