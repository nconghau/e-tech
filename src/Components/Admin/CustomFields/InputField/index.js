import { Checkbox, Form, Input, InputNumber } from "antd"
import { TYPE_CUSTOM_FIELD } from "Constants/Data"

const InputField = props => {
    const {
        key,
        typeinput,
        name,
        label,
        initialValue,
        prefix,
        placeholder,
        suffix,
        button,
        rules,
        hasFeedback = false,
        labelCheckbox,
        type,
        accept,
        value,
        disabled,
        onChange
    } = props
    return (
        <Form.Item
            key={key}
            name={name}
            label={label}
            rules={rules}
            initialValue={initialValue}
            hasFeedback={hasFeedback}
            {...props}
        >
            {typeinput === TYPE_CUSTOM_FIELD.BUTTON ? (
                button
            ) : typeinput === TYPE_CUSTOM_FIELD.INPUT_NUMBER ? (
                <InputNumber />
            ) : typeinput === TYPE_CUSTOM_FIELD.CHECKBOX ? (
                <Checkbox>{labelCheckbox}</Checkbox>
            ) : typeinput === TYPE_CUSTOM_FIELD.PASSWORD ? (
                <Input.Password
                    prefix={prefix}
                    type="password"
                    placeholder={placeholder}
                />
            ) : typeinput === TYPE_CUSTOM_FIELD.TEXTAREA ? (
                <Input.TextArea />
            ) : (
                <Input
                    placeholder={placeholder}
                    value={value}
                    prefix={prefix}
                    suffix={suffix}
                    disabled={disabled}
                    type={type || "text"}
                    accept={accept || ""}
                    onChange={onChange}
                />
            )}
        </Form.Item>
    )
}

export default InputField
