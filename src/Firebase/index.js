import { initializeApp } from "firebase/app"
import { getStorage } from "firebase/storage"
import { getAuth } from "firebase/auth"

const firebaseConfig = {
    apiKey: "AIzaSyDMO_O6_0sZPwiEAu7KVJssIow3jwNlLF0",
    authDomain: "reactdemo-87f11.firebaseapp.com",
    projectId: "reactdemo-87f11",
    storageBucket: "reactdemo-87f11.appspot.com",
    messagingSenderId: "1095291995154",
    appId: "1:1095291995154:web:2e76026f9a0c0ae98004cb",
    measurementId: "G-4G03YSRV3E"
}

const app = initializeApp(firebaseConfig)

const storageFirebase = getStorage(app)

const authentication = getAuth(app)

export { storageFirebase, authentication, app }
