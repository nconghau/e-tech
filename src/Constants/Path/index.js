exports.PATH = {
    BASE: "/etech",
    HOME: "/etech",
    LOGIN: "/dang-nhap",
    SIGNUP: "/dang-ky",
    UPDATE_INFO: "/cap-nhat-tai-khoan",
    FORGOT_PASSWORD: "/quen-mat-khau",
    UPDATE_PASSWORD: "/mat-khau",
    LAPTOP: "/etech/laptop",
    DRIVE: "/etech/drive",
    LAPTOP_DETAIL: "/etech/laptop/:id",
    CHECK_OUT: "/etech/gio-hang",
    ABOUT: "/etech/gioi-thieu",
    DISCOUNT: "/etech/khuyen-mai",
    COOPERATE: "/etech/hop-tac",
    POLICY_TRANSPORT: "/etech/chinh-sach-van-chuyen",
    POLICY_PAY: "/etech/chinh-sach-thanh-toan",
    RECRUITMENT: "/etech/tuyen-dung"
}
