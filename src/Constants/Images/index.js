import Logo from "Assets/Images/Logo.png"
import Logo_Footer from "Assets/Images/Logo_Footer.png"
import Banner_Levono from "Assets/Images/banner-Lenobo-1200x400.png"
import Banner_HP from "Assets/Images/banner-HP-1200x400.png"
import Banner_HP_Envy from "Assets/Images/banner-HP-Envy-1200x400.png"
import Banner_Levono_2 from "Assets/Images/Banner_Levono_2_1200x400.png"
import Logo_Admin from "Assets/Images/Logo_Admin.png"
import QC from "Assets/Images/QC.jpg"
import Store from "Assets/Images/Store.png"
import Banner1 from "Assets/Images/Banner1.jpg"
import Banner2 from "Assets/Images/Banner2.jpg"
import Banner3 from "Assets/Images/Banner3.jpg"
import Discount1 from "Assets/Images/Discount1.jpg"
import Discount2 from "Assets/Images/Discount2.jpg"
import Discount3 from "Assets/Images/Discount3.jpg"
import Discount4 from "Assets/Images/Discount4.jpg"
import Discount5 from "Assets/Images/Discount5.jpg"
import Discount6 from "Assets/Images/Discount6.jpg"
import Discount7 from "Assets/Images/Discount7.jpg"
import Discount8 from "Assets/Images/Discount8.jpg"
import Develop from "Assets/Images/Develop.jpg"
import Dev2 from "Assets/Images/Dev2.jpg"
import Dev3 from "Assets/Images/Dev3.jpg"

const Images = {
    Logo: Logo,
    Banner_HP: Banner_HP,
    Banner_HP_Envy: Banner_HP_Envy,
    Banner_Levono: Banner_Levono,
    Banner_Levono_2: Banner_Levono_2,
    Logo_Footer: Logo_Footer,
    Logo_Admin: Logo_Admin,
    QC: QC,
    Store: Store,
    Banner1: Banner1,
    Banner2: Banner2,
    Banner3: Banner3,
    Discount1: Discount1,
    Discount2: Discount2,
    Discount3: Discount3,
    Discount4: Discount4,
    Discount5: Discount5,
    Discount6: Discount6,
    Discount7: Discount7,
    Discount8: Discount8,
    Develop: Develop,
    Dev2: Dev2,
    Dev3: Dev3
}
export default Images
