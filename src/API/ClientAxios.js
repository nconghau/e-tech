/* eslint-disable no-console */
import axios from "axios"
import Cookie from "js-cookie"
import queryString from "query-string"
const axiosClient = axios.create({
    baseURL: "http://127.0.0.1:9000",
    timeout: 30000,
    headers: {
        "content-type": "application/json",
        "Access-Control-Allow-Origin": "*"
    },
    paramsSerializer: params => queryString.stringify(params)
})

axiosClient.interceptors.request.use(
    config => {
        //Handle token here ...
        const token = Cookie.get("token")
        config.headers.authorization = `Bearer ${token}`

        return config
    },
    err => {
        console.log(err)
    }
)

axiosClient.interceptors.response.use(
    res => {
        console.log("🚀 ~ file: ClientAxios.js ~ line 30 ~ res", res)
        if (res?.data) return res.data

        return res
    },
    err => {
        //Handle err
        console.log(err)
        // window.location.href = "/not-found"
    }
)

export default axiosClient
