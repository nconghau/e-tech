import LoaderComponent from "Components/Web/Common/Loader"
import NotFoundComponent from "Components/Web/Common/NotFound"
import { PATH } from "Constants/Path"
import Authentication from "HOC/BaseAuth"
import LoginPage from "Pages/Common/LoginPage"
import ForgotPasswordPage from "Pages/Common/ForgotPasswordPage"
import SignupPage from "Pages/Common/SignUp"
import UpdateInfoPage from "Pages/Common/UpdateInfo"
import { Suspense } from "react"
import {
    BrowserRouter as Router,
    Redirect,
    Route,
    Switch
} from "react-router-dom"
import RouteAdmin from "Routes/Admin"
import RouteWeb from "Routes/Web"
import UpdatePasswordPage from "Pages/Common/UpdatePasswordPage"

function App() {
    return (
        <div>
            <Suspense fallback={() => <LoaderComponent />}>
                <Router>
                    <Switch>
                        <Redirect exact from="/" to={PATH.HOME} />
                        <Route exact path={PATH.LOGIN} component={LoginPage} />
                        <Route
                            exact
                            path={PATH.SIGNUP}
                            component={SignupPage}
                        />
                        <Route
                            exact
                            path={PATH.UPDATE_INFO}
                            component={UpdateInfoPage}
                        />
                        <Route
                            exact
                            path={PATH.FORGOT_PASSWORD}
                            component={ForgotPasswordPage}
                        />
                        <Route
                            path={`${PATH.UPDATE_PASSWORD}/:type`}
                            component={UpdatePasswordPage}
                        />
                        <Route
                            path={"/admin"}
                            component={Authentication(RouteAdmin, true, true)}
                        />
                        <Route path={PATH.HOME} component={RouteWeb} />
                        <Route
                            render={props => (
                                <NotFoundComponent {...props} isFull={true} />
                            )}
                        />
                    </Switch>
                </Router>
            </Suspense>
        </div>
    )
}

export default App
