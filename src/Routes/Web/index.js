import { BackTop, Result, Spin } from "antd"
import FooterComponent from "Components/Web/Common/Footer"
import HeaderTop from "Components/Web/Common/Header/HeaderTop"
import AboutPage from "Pages/Web/About"
import CooperatePage from "Pages/Web/Cooperate"
import DiscountPage from "Pages/Web/Discount"
import HistoryBill from "Pages/Web/HistoryBill"
import MyAccount from "Pages/Web/MyAccount"
import PolicyPay from "Pages/Web/Policy/Pay"
import PolicyTransport from "Pages/Web/Policy/Transport"
import Recruitment from "Pages/Web/Recruitment"
import React, { useEffect } from "react"
import { useSelector } from "react-redux"
import { Route, Switch, useRouteMatch } from "react-router"
import { LS_NAME, setLS } from "Utils/Converter"
import { emptyItemInLocalStorage } from "Utils/localStorageFunctions"

const HomePage = React.lazy(() => import("../../Pages/Web/Home"))
const ProductDetailPage = React.lazy(() =>
    import("../../Pages/Web/ProductDetail")
)
const CheckOut = React.lazy(() => import("../../Pages/Web/CheckOut"))
const ProductFilter = React.lazy(() => import("../../Pages/Web/ProductFilter"))
const RouteWeb = () => {
    const match = useRouteMatch()
    const loading = useSelector(state => state.SystemReducer.loading)

    useEffect(() => {
        if (emptyItemInLocalStorage(LS_NAME.CARTS)) {
            setLS(LS_NAME.CARTS, [])
        }
    }, [])

    return (
        <Spin spinning={loading} delay={200} tip="Đang tải..." size="large">
            <div>
                <HeaderTop />
                <Switch>
                    <Route
                        exact
                        path={match.url}
                        render={() => {
                            return <HomePage />
                        }}
                    />
                    <Route
                        exact
                        path={`${match.url}/gioi-thieu`}
                        render={() => {
                            return <AboutPage />
                        }}
                    />
                    <Route
                        exact
                        path={`${match.url}/khuyen-mai`}
                        render={() => {
                            return <DiscountPage />
                        }}
                    />
                    <Route
                        exact
                        path={`${match.url}/hop-tac`}
                        render={() => {
                            return <CooperatePage />
                        }}
                    />

                    <Route
                        exact
                        path={`${match.url}/chinh-sach-van-chuyen`}
                        render={() => {
                            return <PolicyTransport />
                        }}
                    />
                    <Route
                        exact
                        path={`${match.url}/chinh-sach-thanh-toan`}
                        render={() => {
                            return <PolicyPay />
                        }}
                    />
                    <Route
                        exact
                        path={`${match.url}/tuyen-dung`}
                        render={() => {
                            return <Recruitment />
                        }}
                    />
                    <Route
                        exact
                        path={`${match.url}/gio-hang`}
                        render={() => {
                            return <CheckOut />
                        }}
                    />
                    <Route
                        exact
                        path={`${match.url}/lich-su`}
                        render={() => {
                            return <HistoryBill />
                        }}
                    />
                    <Route
                        exact
                        path={`${match.url}/tai-khoan`}
                        render={() => {
                            return <MyAccount />
                        }}
                    />
                    <Route
                        path={`${match.url}/:productType/filter/:p`}
                        render={() => {
                            return <ProductFilter />
                        }}
                    />
                    <Route
                        path={`${match.url}/:productType/:productId`}
                        render={() => {
                            return <ProductDetailPage />
                        }}
                    />
                    <Route
                        render={() => (
                            <Result
                                status="404"
                                title="404"
                                subTitle="Xin lỗi, không tìm thấy trang"
                            />
                        )}
                    />
                </Switch>
                <FooterComponent />
                <BackTop />
            </div>
        </Spin>
    )
}
export default RouteWeb
