import { PayPalButtons, PayPalScriptProvider } from "@paypal/react-paypal-js"
import React from "react"
import { convertVNDtoUSD } from "Utils/Converter"
const PaypalComponent = props => {
    const { totalPriceBill, handlePay } = props
    return (
        <div className="paypal-btn">
            {/* sb-t22nu15217437@personal.example.com */}
            {/* HtWM/Pc1 */}
            <h4>Thanh toán trực tuyến</h4>
            <PayPalScriptProvider
                options={{
                    "client-id":
                        "ATIlvXYa_zZbENc1dUdhIYIohLThLgQjAgLIlAMTV1ZHfcoFgYsTIeINSbxafFKzh6OCIYfRgosSSwxu"
                }}
            >
                <PayPalButtons
                    createOrder={(data, actions) => {
                        return actions.order.create({
                            purchase_units: [
                                {
                                    amount: {
                                        value: convertVNDtoUSD(totalPriceBill)
                                    }
                                }
                            ]
                        })
                    }}
                    onApprove={async (data, actions) => {
                        const details = await actions.order.capture()
                        const name = details.payer.name.given_name
                        alert("Đã hoàn thành giao dịch bởi " + name)
                        handlePay()
                    }}
                />
            </PayPalScriptProvider>
        </div>
    )
}
export default React.memo(
    PaypalComponent,
    (prevProps, nextProps) =>
        prevProps.totalPriceBill === nextProps.totalPriceBill
)
