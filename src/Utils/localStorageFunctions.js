const { getLS, setLS, LS_NAME } = require("./Converter")

export const emptyItemInLocalStorage = name => {
    try {
        return Object.keys(getLS(name)).length === 0
    } catch (error) {
        return true
    }
}

export const getTotalPriceOfCartInLocalStorage = () => {
    const carts = getLS(LS_NAME.CARTS)
    let totalPrice = 0
    // let countItem = 0
    carts &&
        carts.map(item => {
            return (totalPrice += item.price)
        })
    return totalPrice
}
export const getQtyOfCartInLocalStorage = () => {
    const carts = getLS(LS_NAME.CARTS)
    return carts.length
}

export const handleSaveCartItem = cartItem => {
    let carts = getLS(LS_NAME.CARTS)
    if (
        !carts.some(item => {
            return item.id === cartItem.id
        })
    ) {
        carts.push(cartItem)
        setLS(LS_NAME.CARTS, carts)
        const reloadCarts = getLS(LS_NAME.RELOAD_CARTS)
        setLS(LS_NAME.RELOAD_CARTS, !reloadCarts)
    }
    return true
}

export const removeCacheLocalStorage = name => {
    return window.localStorage.removeItem(name)
}
