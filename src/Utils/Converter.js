export const LS_NAME = {
    CARTS: "carts",
    RELOAD_CARTS: "reloadCarts",
    PARAMS_REQUEST: "paramsRequest"
}
export const getLS = name => {
    return JSON.parse(window.localStorage.getItem(name))
}

export const setLS = (name, data) => {
    return window.localStorage.setItem(name, JSON.stringify(data))
}

export const changePriceToVND = price => {
    return price?.toLocaleString("it-IT", {
        style: "currency",
        currency: "VND"
    })
}

export const convertVNDtoUSD = price => {
    return String(price / 23000).slice(0, 4)
}
