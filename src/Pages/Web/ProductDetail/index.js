import BreadcrumbComponent from "Components/Web/Breadcrumb"
import ProductDetail from "Components/Web/Product/ProductDetail"
import ProductViewImage from "Components/Web/Product/ProductDetail/ProductViewImage"
import ProductTab from "Components/Web/Product/ProductTab"
import { RENDER_LAPTOP_DETAIL_DEFAULT, TYPE_PRODUCT } from "Constants/Data"
import { useEffect } from "react"
import { useDispatch, useSelector } from "react-redux"
import { useHistory, useParams } from "react-router"
import { getProductDetailApi } from "Redux/Product/Product.thunk"

const ProductDetailPage = () => {
    const { productType, productId } = useParams()
    const history = useHistory()
    const dispatch = useDispatch()

    const productDetail = useSelector(
        state => state.ProductReducer.productDetail
    )

    useEffect(() => {
        window.scrollTo(0, 0)
        if (
            !isNaN(productId) &&
            Object.values(TYPE_PRODUCT).includes(productType)
        ) {
            dispatch(getProductDetailApi(productType, productId)).then(res => {
                if (!res.success) {
                    history.push("/etech/not-found")
                }
            })
        } else {
            history.push("/etech/not-found")
        }
    }, [productId])

    return (
        <div>
            <BreadcrumbComponent
                pageName={
                    productType === TYPE_PRODUCT.LAPTOP
                        ? "Chi tiết Laptop"
                        : "Chi tiết Ổ Cứng"
                }
            />
            <div className="section">
                <div className="container">
                    <div className="row">
                        <div className="product product-details clearfix">
                            <ProductViewImage
                                image={
                                    productDetail?.images ||
                                    RENDER_LAPTOP_DETAIL_DEFAULT.image
                                }
                            />
                            <ProductDetail
                                detail={
                                    productDetail?.info ||
                                    RENDER_LAPTOP_DETAIL_DEFAULT.info
                                }
                                image={
                                    productDetail?.image ||
                                    RENDER_LAPTOP_DETAIL_DEFAULT.image
                                }
                                id={productDetail?.id || 0}
                                discount={productDetail?.discount}
                            />
                            <ProductTab
                                description={
                                    productDetail?.description ||
                                    RENDER_LAPTOP_DETAIL_DEFAULT.description
                                }
                            />
                        </div>
                    </div>
                </div>
            </div>
            {/* {
                <SelectBlock
                    key={0}
                    selectBlockTitle={"Sản phẩm gợi ý"}
                    products={isEmpty(productsFilter) ? null : productsFilter}
                />
            } */}
        </div>
    )
}
export default ProductDetailPage
