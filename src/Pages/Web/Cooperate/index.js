import Images from "Constants/Images"

const CooperatePage = () => {
    window.scrollTo(0, 0)
    return (
        <div className="container">
            <div className="content">
                <br></br>
                <h2>Hợp tác cùng phát triển</h2>
                <p>
                    Tập đoàn Bưu Chính Viễn Thông Việt Nam (VNPT) và Công ty Cổ
                    phần thế giới di động vừa ký kết hợp tác toàn diện; theo đó
                    các cửa hàng/siêu thị của Thế giới di động sẽ trở thành các
                    điểm giao dịch, hoà mạng, cung cấp các dịch vụ
                    VNPT-VinaPhone. Ngược lại, hệ thống cửa hàng VNPT -
                    Vinaphone là nơi cung cấp các thiết bị đầu cuối của thế giới
                    di động.
                </p>
                <img src={Images.Develop} alt="" className="img75" />
                <p>
                    Tại buổi ký kết, hai bên cam kết sự hợp tác nhằm mang đến
                    cho khách hàng những gói dịch vụ Vinaphone và các dòng điện
                    thoại Samsung rất khác biệt chỉ có tại hệ thống cửa hàng
                    VNPT-VinaPhone và chuỗi cửa hàng Thế giới di động trên toàn
                    quốc về chính sách giá, khuyền mãi, hậu mãi …. Trong năm
                    2014, Công ty Cổ phần Thế giới di động đã phối hợp với VNPT
                    TP HCM – Đơn vị thành viên của Tập đoàn Bưu chính Viễn thông
                    Việt Nam thực hiện chương trình hợp tác triển khai bán các
                    gói sản phẩm, dịch vụ di động Vinaphone có tiêu chí “Gói
                    cước Vinaphone chỉ có tại Thế giới di động” trên toàn bộ hệ
                    thống cửa hàng, siêu thị của Thế giới di động trên toàn quốc
                    và đặc biệt là tại 69 cửa hàng tại TP HCM tạo được sự thuận
                    lợi cho khách hàng. Với những kết quả ban đầu đã đạt được,
                    Tập đoàn Bưu chính Viễn thông Việt Nam tiếp tục hợp tác với
                    Công ty Cổ phần Thế giới di động để triển khai nhiều dự án
                    hợp tác khác, đem lại những lợi ích thiết thực cho cả hai
                    bên và đem đến cho khách hàng được nhiều thuận lợi và tiện
                    ích khi sử dụng dịch vụ viễn thông của VNPT. Đây cũng là xu
                    hướng trên thế giới về sự hợp tác giữa nhà mạng cung cấp
                    dịch vụ Viễn thông và các doanh nghiệp, chuỗi cửa hàng, siêu
                    thị bán lẻ thiết bị viễn thông.
                </p>
                <img src={Images.Dev2} alt="" className="img75" />
                <p>
                    Hiện tại, phân khúc tivi phổ thông vẫn còn đang bỏ ngỏ do
                    các thương hiệu nước ngoài tập trung vào phân khúc trung cấp
                    và cận cao cấp, nhu cầu của người dùng Việt cho sản phẩm
                    tivi chất lượng nhưng giá bình dân là vô cùng lớn. Điển hình
                    là thử nghiệm đặt trước tivi Sanco trên trang web Điện máy
                    XANH chỉ trong 05 ngày đã “cháy hàng” với gần 1000 lượt đặt
                    hàng, do đó Sanco càng tin tưởng hơn vào sự hợp tác với Điện
                    máy XANH trong thời gian sắp tới sẽ đưa sản phẩm tivi phủ
                    khắp các tỉnh thành và đến tay người tiêu dùng một cách
                    nhanh nhất.
                </p>
                <img src={Images.Dev3} alt="" className="img75" />
            </div>
        </div>
    )
}

export default CooperatePage
