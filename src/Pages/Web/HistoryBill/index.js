import { Statistic, Table } from "antd"
import BreadcrumbComponent from "Components/Web/Breadcrumb"
import { useEffect, useState } from "react"
import { useDispatch } from "react-redux"
import { getBillsHistoryApi } from "Redux/Cart/Cart.thunk"
import { changePriceToVND } from "Utils/Converter"
import "./History.css"
const HistoryBill = () => {
    const dispatch = useDispatch()
    const [billDetail, setBillDetail] = useState(null)

    useEffect(() => {
        dispatch(getBillsHistoryApi()).then(res => setBillDetail(res.data))
    }, [])

    const changeStatusVN = status => {
        switch (status) {
            case "AWAIT_FOR_CONFIRMATION":
                return "Đang chờ xác nhận"
            case "ON_GOING":
                return "Đang giao"
            case "DELIVERED":
                return "Đã giao"
            case "CANCELLED":
                return "Hủy đơn"
            default:
                return "-----"
        }
    }

    const getTitle = item => {
        return (
            <b>
                <p>{`Mã hóa đơn: ${item?.billId}`}</p>
                <p>{`Thời gian mua: ${
                    item?.bill?.timeBuy &&
                    new Date(item?.bill?.timeBuy).toLocaleString("en-GB")
                }`}</p>
                <p>{item?.note && `Ghi chú: ${item?.note}`}</p>
                <div className="statistic">
                    <Statistic
                        title="Trạng thái đơn hàng"
                        value={changeStatusVN(item?.status)}
                        style={{
                            marginRight: 32
                        }}
                    />
                    <Statistic
                        title="Tổng đơn"
                        value={
                            item?.bill?.totalPrice &&
                            changePriceToVND(item?.bill?.totalPrice)
                        }
                    />
                </div>
            </b>
        )
    }

    const columnsProduct = [
        {
            title: "Mã SP",
            dataIndex: "id",
            key: "id",
            align: "center"
        },
        {
            title: "Sản phẩm",
            dataIndex: "name",
            key: "name",
            align: "center",
            width: 180
        },
        {
            title: "Ảnh",
            dataIndex: "image",
            key: "image",
            render: image => <img alt="" src={image} width="100%"></img>,
            width: 160,
            align: "center"
        },
        {
            title: "Giá",
            dataIndex: "price",
            key: "price",
            render: price => changePriceToVND(price),
            align: "center"
        },
        {
            title: "Số lượng",
            dataIndex: "qty",
            key: "qty",
            align: "center"
        }
    ]
    return (
        <div>
            <BreadcrumbComponent pageName={"Lịch sử đặt hàng"} />
            <br />
            <div className="container">
                {billDetail?.map(item => {
                    return (
                        <Table
                            key={item.billId}
                            columns={columnsProduct}
                            pagination={false}
                            dataSource={item?.bill?.products}
                            title={() => getTitle(item && item)}
                            footer={() => <p></p>}
                            scroll={{ x: 400 }}
                        />
                    )
                })}
                <br />
                {billDetail ? (
                    <h4>Cảm ơn bạn đã đặt hàng!</h4>
                ) : (
                    <h4>Bạn chưa có hóa đơn nào, hãy đặt hàng ngay nhé!</h4>
                )}
            </div>
        </div>
    )
}

export default HistoryBill
