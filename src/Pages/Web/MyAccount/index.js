import { FormOutlined } from "@ant-design/icons"
import { Card, notification } from "antd"
import BreadcrumbComponent from "Components/Web/Breadcrumb"
import { PATH } from "Constants/Path"
import Cookie from "js-cookie"
import React, { useState } from "react"
import { useDispatch } from "react-redux"
import { useHistory } from "react-router-dom"
import { logoutUser } from "Redux/User/User.thunk"
import "./MyAccount.css"
const MyAccount = () => {
    const [account, setAccount] = useState(Cookie.getJSON("account") || {})
    const dispatch = useDispatch()
    const history = useHistory()

    // const logout = () => {
    //     dispatch(logoutUser())
    //     history.push(PATH.HOME)
    // }

    return (
        <div>
            <BreadcrumbComponent pageName={"Tài khoản của tôi"} />
            <div className="container">
                <div className="myaccount">
                    <Card
                        className="myaccount-avatar"
                        hoverable
                        style={{ width: 220 }}
                        cover={
                            <img
                                alt=""
                                src={
                                    "https://png.pngtree.com/png-clipart/20190925/original/pngtree-no-avatar-vector-isolated-on-white-background-png-image_4979074.jpg"
                                }
                            />
                        }
                    ></Card>
                    <div className="myaccount-info">
                        <p>
                            Tài khoản:&ensp;
                            {account?.name}
                        </p>
                        <p>Số điện thoại:&ensp;{account?.phone}</p>
                        <p>Email:&ensp;{account?.email}</p>
                        <p>Địa chỉ:&ensp;{account?.address}</p>
                        <br />
                        <div>
                            {/* <Button type="primary" icon={<FormOutlined />}>
                           
                        </Button> */}
                            <button
                                className="primary-btn"
                                onClick={() => history.push(PATH.UPDATE_INFO)}
                            >
                                <FormOutlined /> Cập nhật tài khoản
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default MyAccount
