import Images from "Constants/Images"

const DiscountPage = () => {
    window.scrollTo(0, 0)
    return (
        <div className="container">
            <div className="content">
                <br></br>
                <h2>Khuyến mãi</h2>
                <div className="content-title">
                    1. Chương trình khuyến mãi Laptop DELL{" "}
                </div>
                <p>
                    <img src={Images.Develop} alt="" className="img75" />
                </p>
                <p>
                    Tên chương trình khuyến mãi: “CÙNG DELL TRỞ LẠI, NGHE GỌI
                    THẢ GA” <br></br>
                    Sản phẩm, dịch vụ khuyến mãi: Mua máy tính xách tay, máy
                    tính để bàn chính hãng Dell™ thuộc các dòng sản phẩm
                    Inspiron, Vostro, XPS, Gaming có cài sẵn Windows 10{" "}
                    <br></br>
                    Chương trình không áp dụng cho các dòng sản phẩm thuộc kênh
                    dự án hoặc đang được bán lẻ trên thị trường: Latitude,
                    Optiplex, Precision... <br></br>
                    Thời gian khuyến mãi: từ 24/05/2020 đến 03/06/2020.{" "}
                    <br></br>
                    Phạm vi khuyến mãi: các đại lý uỷ quyền của Dell™ trên toàn
                    quốc <br></br>
                    Đối tượng khuyến mãi: Người tiêu dùng cuối khi mua máy tính
                    xách tay, máy tính để bàn chính hãng của Dell™ trong thời
                    gian khuyến mãi. <br></br>
                    Hình thức khuyến mãi: <br></br>+ Khách hàng khi mua sản phẩm
                    máy tính chính hãng Dell™ thuộc các dòng sản phẩm Inspiron,
                    Vostro, XPS, Gaming có cài sẵn Windows 10 được phân phối
                    chính hãng qua kênh bán lẻ từ các nhà phân phối Digiworld
                    (DGW), PSD, FPT, ADG, VSC (hay NPP Thủy Linh khu vực phía
                    Bắc) sẽ được sở hữu ngay: 01 (một) thẻ cào điện thoại trị
                    giá 300,000VNĐ. <br></br>+ Chương trình có thể kết thúc sớm
                    hơn khi hết quà. <br></br>
                </p>
                <div className="content-title">
                    2. Chương trình khuyến mãi Laptop HP
                </div>
                <p>
                    <img src={Images.Discount2} alt="" className="img75" />
                </p>
                <div className="content-title">
                    3. Chương trình khuyến mãi Laptop Asus
                </div>
                <p>
                    <img src={Images.Discount3} alt="" className="img75" />
                    <img src={Images.Discount4} alt="" className="img75" />
                </p>
                <div className="content-title">
                    4. Chương trình khuyến mãi Laptop Acer{" "}
                </div>
                <p>
                    <img src={Images.Discount5} alt="" className="img75" />
                </p>
                <div className="content-title">
                    5. Chương trình khuyến mãi Laptop Lenovo
                </div>
                <p>
                    <img src={Images.Discount6} alt="" className="img75" />
                </p>

                <div className="content-title">
                    6. Chương trình khuyến mãi Laptop MSI
                </div>
                <p>
                    <img src={Images.Discount7} alt="" className="img75" />
                    <img src={Images.Discount8} alt="" className="img75" />
                </p>
                <p>
                    Liên tục cập nhật chương trình khuyến mãi Laptop Mới và Hot
                    nhất...<br></br>
                    Vào ETech săn chương khuyến mãi để được giảm giá cùng nhiều
                    quà tặng.<br></br>
                    Kính chúc Quý Anh/Chị Sức khỏe - Thành đạt!<br></br>
                </p>
            </div>
        </div>
    )
}

export default DiscountPage
