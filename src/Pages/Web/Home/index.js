import HeaderBanner from "Components/Web/Common/Header/HeaderBanner"
// import SelectBlock from "Components/Web/Product/ProductShow/SelectBlock"
import React, { useEffect } from "react"
import { useDispatch, useSelector } from "react-redux"
import { getCartLS } from "Redux/Cart/Cart.thunk"
import { getProductsApi } from "Redux/Product/Product.thunk"
import { LS_NAME, setLS } from "Utils/Converter"
import { emptyItemInLocalStorage } from "Utils/localStorageFunctions"
const SelectBlock = React.lazy(() =>
    import("../../../Components/Web/Product/ProductShow/SelectBlock")
)
const HomePage = () => {
    const dispatch = useDispatch()

    useEffect(() => {
        if (emptyItemInLocalStorage(LS_NAME.CARTS)) {
            setLS(LS_NAME.CARTS, [])
        }
        dispatch(getCartLS())
        dispatch(getProductsApi())
    }, [])
    const products = useSelector(state => state.ProductReducer.products)

    return (
        <div>
            <HeaderBanner />

            {products?.top && (
                <SelectBlock
                    key={"top"}
                    selectBlockTitle={"Laptop bán chạy"}
                    products={products?.top?.result}
                />
            )}

            {products?.new && (
                <SelectBlock
                    key={"new"}
                    selectBlockTitle={"Sản phẩm mới"}
                    products={products?.new?.result}
                />
            )}

            {products?.brand &&
                products?.brand.map(item => {
                    return (
                        <SelectBlock
                            key={item?.id}
                            selectBlockTitle={item?.title}
                            products={item?.result}
                        />
                    )
                })}
            {/* <BenefitComponent /> */}
        </div>
    )
}

export default HomePage
