import { Area } from "@ant-design/charts"
import { LikeOutlined } from "@ant-design/icons"
import { Card, Col, Row, Statistic } from "antd"
import BreadcrumbField from "Components/Admin/CustomFields/BreadcrumbField"
import React from "react"
import { useDispatch, useSelector } from "react-redux"
import { getStatistic } from "Redux/Admin/Statistic/Statistic.thunk"

const StatisticPage = () => {
    const dispatch = useDispatch()
    const data = useSelector(state => state.StatisticReducer.data)
    const number = useSelector(state => state.StatisticReducer.number)
    React.useEffect(() => {
        dispatch(getStatistic())
    }, [])
    const config = {
        data,
        xField: "month",
        yField: "total",
        point: {
            size: 5,
            shape: "diamond"
        },
        annotations: [
            {
                type: "text",
                position: ["min", "median"],
                content: "Mức doanh thu ổn",
                // offsetY: -4,
                style: { textBaseline: "bottom" }
            },
            {
                type: "line",
                start: ["min", "median"],
                end: ["max", "median"]
            }
        ]
    }
    return (
        <>
            <BreadcrumbField list={["Admin", "Thống kê"]} />
            <Row gutter={16}>
                <Col span={12}>
                    <Card>
                        <Statistic
                            title="Tổng số khách hàng đăng ký mới"
                            value={number?.newUserInMonth}
                            prefix={<LikeOutlined />}
                        />
                    </Card>
                </Col>
                <Col span={12}>
                    <Card>
                        <Statistic
                            title="Tổng số khách hàng trong hệ thống"
                            value={number?.totalUser}
                            prefix={<LikeOutlined />}
                        />
                    </Card>
                </Col>
                <Col span={12}>
                    <Card>
                        <Statistic
                            title="Tổng đơn hàng mới trong tháng"
                            value={number?.newOrderInMonth}
                            prefix={<LikeOutlined />}
                        />
                    </Card>
                </Col>
                <Col span={12}>
                    <Card>
                        <Statistic
                            title="Tổng số đơn hàng"
                            value={number?.totalOrder}
                            prefix={<LikeOutlined />}
                        />
                    </Card>
                </Col>
            </Row>
            <Area {...config} />
        </>
    )
}

export default StatisticPage
