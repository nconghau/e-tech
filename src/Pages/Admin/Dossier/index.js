import {
    CloseOutlined,
    DeleteOutlined,
    FileSearchOutlined,
    FormOutlined,
    LinkOutlined,
    PercentageOutlined,
    ReloadOutlined,
    SaveOutlined
} from "@ant-design/icons"
import {
    Button,
    DatePicker,
    Drawer,
    Form,
    InputNumber,
    Layout,
    notification,
    Space,
    Statistic,
    Table,
    Tag,
    Tooltip
} from "antd"
import Search from "antd/lib/input/Search"
import BreadcrumbField from "Components/Admin/CustomFields/BreadcrumbField"
import { TYPE_PRODUCT } from "Constants/Data"
import { PATH } from "Constants/Path"
import { Markup } from "interweave"
import { useEffect, useState } from "react"
import { useDispatch, useSelector } from "react-redux"
import { Link, useParams, useRouteMatch } from "react-router-dom"
import { handleSearchByName } from "Redux/Admin/Product/ProductAdmin.reducer"
import {
    getProductsApi,
    updateDiscount
} from "Redux/Admin/Product/ProductAdmin.thunk"
import { changePriceToVND } from "Utils/Converter"
import moment from "moment"
import "./Dossier.css"
const { RangePicker } = DatePicker

const { Content } = Layout

const DossierData = () => {
    const { productType } = useParams()
    const max_page = useSelector(state => state.ProductAdminReducer.max_page)
    const curr_page = useSelector(state => state.ProductAdminReducer.curr_page)
    const productsFilter = useSelector(
        state => state.ProductAdminReducer.productsFilter
    )
    const filters = useSelector(state => state.ProductAdminReducer.filters)
    const match = useRouteMatch()
    const dispatch = useDispatch()
    const [openDescription, setOpenDescription] = useState(false)
    const [description, setDescription] = useState("")
    const [discountProductName, setDiscountProductName] = useState("")
    const [discountProductId, setDiscountProductId] = useState(0)
    const [price, setPrice] = useState(0)
    const [discountPrice, setDiscountPrice] = useState(0)
    const [discountPercent, setDiscountPercent] = useState(0)
    const [openDiscount, setOpenDiscount] = useState(false)

    //---------------------CALL API------------------

    useEffect(() => {
        dispatch(getProductsApi(productType, 1))
    }, [productType])

    const handleGetProducts = (page, pageSize) => {
        dispatch(getProductsApi(productType, page))
    }

    // ---------------------EVEN---------------------

    const handleUpdateDiscount = values => {
        const bodyDiscount = {
            product_id: discountProductId,
            percent: values?.percent,
            start_date: values?.discountTime[0].format("YYYY-MM-DD HH:mm"),
            end_date: values?.discountTime[1].format("YYYY-MM-DD HH:mm")
        }
        const response = dispatch(updateDiscount(bodyDiscount))
        response.then(res => {
            if (res?.success) {
                notification.success({
                    message: "Thông báo",
                    description: "Đã cập nhật khuyến mãi"
                })
            } else {
                notification.error({
                    message: "Thông báo cập nhật không thành công",
                    description:
                        "Vui lòng kiểm tra thời gian hoặc sản phẩm còn trong thời gian khuyến mãi"
                })
            }
            setOpenDiscount(false)
        })
    }

    const handleDelete = key => {
        console.log(key)
    }

    const reloadData = () => {
        dispatch(getProductsApi(productType, 1))
    }

    const handleShowDescription = description => {
        setOpenDescription(true)
        setDescription(description)
    }

    const handleShowDiscountForm = record => {
        setDiscountProductId(record?.id)
        setDiscountProductName(record?.name)
        setPrice(record?.price)
        setDiscountPrice(record?.discount_price)
        setDiscountPercent(record?.discount_percent)
        setOpenDiscount(true)
    }

    // ---------------------COLUMNS---------------------
    const columnsLaptop = [
        {
            key: "name",
            title: "Tên sản phẩm",
            dataIndex: "name",
            width: 220,
            fixed: "left"
        },
        {
            key: "image",
            title: "Ảnh",
            dataIndex: "image",
            render: image => <img alt="" src={image} width="100%"></img>,
            width: 120,
            fixed: "left"
        },
        {
            key: "discount_price",
            title: "Giá khuyến mãi",
            dataIndex: "discount_price",
            render: (value, record) => (
                <div>
                    {record?.discount_price !== 0
                        ? record?.discount_price
                        : "--"}{" "}
                    {record?.discount_percent !== 0 && (
                        <Tag color={"green"} key={record?.id}>
                            -{record?.discount_percent}%
                        </Tag>
                    )}
                </div>
            ),
            width: 150
        },
        {
            key: "price",
            title: "Giá bán",
            dataIndex: "price",
            sorter: (a, b) => a.price - b.price,
            render: price => changePriceToVND(price),
            width: 150
        },
        {
            key: "id",
            title: "Mã SP",
            dataIndex: "id",
            sorter: (a, b) => a.id - b.id,
            defaultSortOrder: "descend",
            width: 100
        },
        {
            key: "brand",
            title: "Hãng",
            dataIndex: "brand",
            filters: filters?.Brand,
            onFilter: (value, record) => record.brand.includes(value),
            width: 100
        },
        {
            key: "cpu",
            title: "Vi xử lý",
            dataIndex: "cpu",
            filters: filters?.Cpu,
            onFilter: (value, record) =>
                record.cpu ? record.cpu.includes(value) : "",
            width: 180
        },
        {
            key: "ram",
            title: "Ram",
            dataIndex: "ram",
            filters: filters?.Ram,
            onFilter: (value, record) => record.ram.includes(value),
            width: 180
        },
        {
            key: "rom",
            title: "Lưu trữ",
            dataIndex: "rom",
            filters: filters?.Rom,
            onFilter: (value, record) => record.rom.includes(value),
            width: 180
        },
        {
            key: "action",
            title: "Chức năng",
            dataIndex: "action",
            width: 160,
            align: "center",
            fixed: "right",
            render: (value, record) => (
                <Space size={"small"}>
                    <Link to={`${match.url}/${record.id}`} key={record.id}>
                        <Tooltip title="Cập nhật sản phẩm">
                            <Button
                                style={{
                                    background: "#b37feb",
                                    borderColor: "#b37feb"
                                }}
                                type="primary"
                                icon={<FormOutlined />}
                            ></Button>
                        </Tooltip>
                    </Link>
                    <Tooltip title="Xem mô tả">
                        <Button
                            type="primary"
                            style={{
                                background: "#f759ab",
                                borderColor: "#f759ab"
                            }}
                            icon={<FileSearchOutlined />}
                            onClick={() =>
                                handleShowDescription(record.description)
                            }
                        ></Button>
                    </Tooltip>
                    <Tooltip title="Khuyến mãi">
                        <Button
                            type="primary"
                            style={{
                                background: "#52c41a",
                                borderColor: "#52c41a"
                            }}
                            icon={<PercentageOutlined />}
                            onClick={() => handleShowDiscountForm(record)}
                        ></Button>
                    </Tooltip>
                    <Link to={`${PATH.HOME}/laptop/${record.id}`}>
                        <Tooltip title="Xem trên website">
                            <Button
                                type="primary"
                                icon={<LinkOutlined />}
                            ></Button>
                        </Tooltip>
                    </Link>
                    <Link to={`/${record.id}`} key={record.id}>
                        <Button
                            danger
                            icon={<DeleteOutlined />}
                            onClick={any => handleDelete}
                            disabled
                        />
                    </Link>
                </Space>
            )
        }
    ]
    const columnsDrive = [
        {
            key: "name",
            title: "Tên sản phẩm",
            dataIndex: "name",
            width: 200,
            fixed: "left"
        },
        {
            key: "image",
            title: "Ảnh",
            dataIndex: "image",
            render: image => <img alt="" src={image} width="100%"></img>,
            width: 100,
            fixed: "left"
        },
        {
            key: "discount_price",
            title: "Giá khuyến mãi",
            dataIndex: "discount_price",
            render: (value, record) => (
                <div>
                    {record?.discount_price !== 0
                        ? record?.discount_price
                        : "--"}{" "}
                    {record?.discount_percent !== 0 && (
                        <Tag color={"green"} key={record?.id}>
                            -{record?.discount_percent}%
                        </Tag>
                    )}
                </div>
            ),
            width: 150
        },
        {
            key: "price",
            title: "Giá bán",
            dataIndex: "price",
            sorter: (a, b) => a.price - b.price,
            render: price => changePriceToVND(price),
            width: 150
        },

        {
            key: "id",
            title: "Mã SP",
            dataIndex: "id",
            sorter: (a, b) => a.id - b.id,
            defaultSortOrder: "descend",
            width: 80
        },
        {
            title: "Hãng",
            dataIndex: "brand",
            filters: filters?.Brand,
            onFilter: (value, record) => record.brand.includes(value),
            width: 100
        },

        {
            key: "type",
            title: "Loại ổ cứng",
            dataIndex: "type",
            filters: filters?.type,
            onFilter: (value, record) => record.type.includes(value),
            width: 120
        },
        {
            key: "capacity",
            title: "Dung lượng",
            dataIndex: "capacity",
            filters: filters?.capacity,
            onFilter: (value, record) => record.capacity.includes(value),
            width: 120
        },
        {
            key: "created_at",
            title: "Ngày nhập kho",
            dataIndex: "created_at",
            sorter: (a, b) => a.price - b.price,
            render: created_at => new Date(created_at).toLocaleString("en-GB"),
            width: 150
        },
        {
            key: "updated_at",
            title: "Cập nhật mới nhất",
            sorter: (a, b) => a.price - b.price,
            render: updated_at => new Date(updated_at).toLocaleString("en-GB"),
            dataIndex: "updated_at",
            width: 150
        },
        {
            key: "action",
            title: "Chức năng",
            dataIndex: "action",
            width: 160,
            align: "center",
            fixed: "right",
            render: (value, record) => (
                <Space size={"small"}>
                    <Link to={`${match.url}/${record.id}`} key={record.id}>
                        <Tooltip title="Cập nhật sản phẩm">
                            <Button
                                style={{
                                    background: "#b37feb",
                                    borderColor: "#b37feb"
                                }}
                                type="primary"
                                icon={<FormOutlined />}
                            ></Button>
                        </Tooltip>
                    </Link>
                    <Tooltip title="Xem mô tả">
                        <Button
                            type="primary"
                            style={{
                                background: "#f759ab",
                                borderColor: "#f759ab"
                            }}
                            icon={<FileSearchOutlined />}
                            onClick={() =>
                                handleShowDescription(record.description)
                            }
                        ></Button>
                    </Tooltip>
                    <Tooltip title="Khuyến mãi">
                        <Button
                            type="primary"
                            style={{
                                background: "#52c41a",
                                borderColor: "#52c41a"
                            }}
                            icon={<PercentageOutlined />}
                            onClick={() => handleShowDiscountForm(record)}
                        ></Button>
                    </Tooltip>
                    <Link to={`${PATH.HOME}/drive/${record.id}`}>
                        <Tooltip title="Xem trên website">
                            <Button
                                type="primary"
                                icon={<LinkOutlined />}
                            ></Button>
                        </Tooltip>
                    </Link>
                    <Link to={`/${record.id}`} key={record.id}>
                        <Button
                            danger
                            icon={<DeleteOutlined />}
                            onClick={any => handleDelete}
                            disabled
                        />
                    </Link>
                </Space>
            )
        }
    ]

    return (
        <Layout className="site-layout">
            <BreadcrumbField
                list={[
                    "Admin",
                    productType === TYPE_PRODUCT.LAPTOP
                        ? "Danh sách Laptop"
                        : "Danh sách Ổ Cứng"
                ]}
            />
            <Content
                style={{
                    overflow: "initial"
                }}
            >
                <Button
                    type="primary"
                    style={{ marginBottom: 5, width: 80 }}
                    icon={<ReloadOutlined />}
                    onClick={() => reloadData()}
                />
                <Search
                    placeholder="Tìm kiếm tên sản phẩm"
                    style={{ width: 200, marginLeft: 5 }}
                    onChange={e => {
                        dispatch(handleSearchByName(e.target.value))
                    }}
                />
                {/* Show Description */}
                <Drawer
                    title="Mô tả sản phẩm"
                    width={"60%"}
                    closable={true}
                    onClose={() => setOpenDescription(false)}
                    visible={openDescription}
                >
                    <div>
                        <Markup content={description} />
                    </div>
                </Drawer>
                {/* Show Form Set Discount */}
                <Drawer
                    title="Khuyến mãi của sản phẩm"
                    width={"40%"}
                    closable={true}
                    onClose={() => setOpenDiscount(false)}
                    visible={openDiscount}
                >
                    <Form
                        name="nest-messages"
                        onFinish={values => handleUpdateDiscount(values)}
                    >
                        <Statistic
                            title="Sản phẩm"
                            value={discountProductName && discountProductName}
                        />
                        <Statistic
                            title="Giá gốc"
                            value={price && changePriceToVND(price)}
                        />
                        <Statistic
                            title="Giá khuyến mãi"
                            value={
                                discountPrice && changePriceToVND(discountPrice)
                            }
                        />
                        <br></br>
                        <h4>Phần trăm khuyến mãi</h4>
                        <Form.Item name={"percent"}>
                            <InputNumber
                                size="large"
                                min={0}
                                max={99}
                                value={discountPercent && discountPercent}
                                onChange={e =>
                                    setDiscountPrice(price - (price * e) / 100)
                                }
                            />
                        </Form.Item>
                        <h4>Thời gian khuyến mãi</h4>
                        <Form.Item name={"discountTime"}>
                            <RangePicker
                                showTime={{ format: "HH:mm" }}
                                format="YYYY-MM-DD HH:mm"
                                value={[
                                    moment("2022-04-29 20:22"),
                                    moment("2022-04-30 20:22")
                                ]}
                            />
                        </Form.Item>

                        <br></br>
                        <Space size={"small"}>
                            <Button
                                type="primary"
                                htmlType="submit"
                                icon={<SaveOutlined />}
                            >
                                Cập nhật
                            </Button>

                            <Button
                                text="Hủy"
                                htmlType="cancel"
                                icon={<CloseOutlined />}
                                onClick={() => setOpenDiscount(false)}
                            >
                                Hủy
                            </Button>
                        </Space>
                    </Form>
                </Drawer>
                {/* Table */}
                <Form component={false}>
                    <Table
                        bordered
                        dataSource={productsFilter}
                        columns={
                            productType === "laptop"
                                ? columnsLaptop
                                : productType === "drive"
                                ? columnsDrive
                                : []
                        }
                        pagination={{
                            defaultPageSize: 15,
                            current: curr_page,
                            total: max_page * 15,
                            onChange: (page, pageSize) => {
                                handleGetProducts(page, pageSize)
                            },
                            pageSizeOptions: []
                        }}
                        scroll={{ x: 1800, y: 650 }}
                        // footer={() => "Footer"}
                    />
                </Form>
            </Content>
        </Layout>
    )
}
export default DossierData
