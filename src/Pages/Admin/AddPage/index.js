import {
    CloseOutlined,
    EditOutlined,
    LinkOutlined,
    PercentageOutlined,
    SaveOutlined
} from "@ant-design/icons"
import ClassicEditor from "@ckeditor/ckeditor5-build-classic"
import { CKEditor } from "@ckeditor/ckeditor5-react"
import {
    Button,
    Col,
    DatePicker,
    Drawer,
    Form,
    InputNumber,
    notification,
    Row,
    Space,
    Statistic
} from "antd"
import BreadcrumbField from "Components/Admin/CustomFields/BreadcrumbField"
import InputField from "Components/Admin/CustomFields/InputField"
import SelectField from "Components/Admin/CustomFields/SelectField"
import { LIST_RENDER_DEFAULT, TYPE_CUSTOM_FIELD } from "Constants/Data"
import { VALIDATE_MESSAGES } from "Constants/Validate"
import { storageFirebase } from "Firebase"
import { FIREBASE_PATH } from "Firebase/path"
import { getDownloadURL, ref, uploadBytesResumable } from "firebase/storage"
import React from "react"
import { useDispatch, useSelector } from "react-redux"
import { useHistory } from "react-router-dom"
import { setIsSSDForCreateDrive } from "Redux/Admin/Product/ProductAdmin.reducer"
import {
    createProductsApi,
    getSpecListApi
} from "Redux/Admin/Product/ProductAdmin.thunk"
import { changePriceToVND } from "Utils/Converter"
import "../AddEditPage.css"
const { RangePicker } = DatePicker
const layout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 12 }
}

const AddProductPage = () => {
    const dispatch = useDispatch()
    const history = useHistory()
    const filterAll = useSelector(state => state.ProductAdminReducer.filterAll)
    const isSSDForCreateDrive = useSelector(
        state => state.ProductAdminReducer.isSSDForCreateDrive
    )
    const [cancel, setCancel] = React.useState(false)
    const [openDescription, setOpenDescription] = React.useState(false)
    const [openDiscount, setOpenDiscount] = React.useState(false)
    const [description, setDescription] = React.useState("")
    const [productTypeIdForCreate, setProductTypeIdForCreate] = React.useState(
        1
    )
    const [price, setPrice] = React.useState(0)
    const [discountPrice, setDiscountPrice] = React.useState(0)
    const [discountPercent, setDiscountPercent] = React.useState(0)
    const [discountTime, setDiscountTime] = React.useState([])

    //----------------------GET DATA SHOW SELECT FIELDS----------
    React.useEffect(() => {
        dispatch(getSpecListApi())
    }, [])

    //------------------------NOTIFY-----------------------------
    const openNotify = (type, title, message) => {
        notification[type]({
            message: title,
            description: message
        })
    }

    //------------------------SUBMIT------------------------------
    const onCancel = () => {
        setCancel(true)
        window.location.href = "/admin/home"
    }

    const onFinish = values => {
        const typeId = Number(values.type_id)
        if (!cancel) {
            if (typeId === 1) {
                const bodyLaptop = {
                    info: {
                        name: values.name,
                        description: description,
                        guarantee: Number(values.guarantee),
                        price: Number(values.price),
                        brand_id: Number(values.brand_id),
                        status_id: 1,
                        type_id: typeId
                    },
                    spec: {
                        cpu_id: Number(values.cpu_id),
                        gpu_id: Number(values.gpu_id),
                        ram_id: Number(values.ram_id),
                        size_id: Number(values.size_id),
                        rom_id: Number(values.rom_id),
                        screen_id: Number(values.screen_id),
                        port_id: Number(values.port_id),
                        os_id: Number(values.os_id),
                        battery_id: Number(values.battery_id),
                        weight_id: Number(values.weight_id)
                    },
                    image: {
                        img1: values.img1,
                        img2: values.img2,
                        img3: values.img3
                    }
                }
                if (
                    discountPercent > 0 &&
                    discountTime.length === 2 &&
                    !discountTime.includes("")
                ) {
                    bodyLaptop.discount = {
                        percent: discountPercent,
                        start_date: discountTime[0],
                        end_date: discountTime[1]
                    }
                }
                dispatch(createProductsApi("laptop", bodyLaptop)).then(notify =>
                    openNotify(notify.type, notify.title, notify.message)
                )
                history.push("/admin/products/laptop")
                return
            }
            if (typeId === 2) {
                const bodyDrive = {
                    info: {
                        name: values.name,
                        description: description,
                        guarantee: Number(values.guarantee),
                        price: Number(values.price),
                        brand_id: Number(values.brand_id),
                        status_id: 1,
                        type_id: typeId
                    },
                    spec: {
                        capacity_id: Number(values.capacity_id),
                        cache_id: isSSDForCreateDrive
                            ? 1
                            : Number(values.cache_id),
                        connect_id: Number(values.connect_id),
                        write_id: Number(values.write_id),
                        read_id: Number(values.read_id),
                        dimension_id: Number(values.dimension_id),
                        rotation_id: isSSDForCreateDrive
                            ? 1
                            : Number(values.rotation_id),
                        drive_type_id: Number(values.drive_type_id)
                    },
                    image: {
                        img1: values.img1,
                        img2: values.img2,
                        img3: values.img3
                    }
                }
                if (
                    discountPercent > 0 &&
                    discountTime.length === 2 &&
                    !discountTime.includes("")
                ) {
                    bodyDrive.discount = {
                        percent: discountPercent,
                        start_date: discountTime[0],
                        end_date: discountTime[1]
                    }
                }
                dispatch(createProductsApi("drive", bodyDrive)).then(notify =>
                    openNotify(notify.type, notify.title, notify.message)
                )
                history.push("/admin/products/drive")
            } else {
                openNotify(
                    "info",
                    "Thêm chưa thành công!",
                    "Tính năng đang cập nhật"
                )
            }
        }
    }

    //------------------------COMPONENT RENDER---------------------
    const renderSpecLaptop = () => {
        return (
            <Col span={12}>
                <SelectField
                    name={"cpu_id"}
                    label={"Vi xử lý"}
                    options={filterAll?.laptop?.cpus || LIST_RENDER_DEFAULT}
                    rules={[{ required: true }]}
                />
                <SelectField
                    name={"ram_id"}
                    label={"Ram"}
                    options={filterAll?.laptop?.rams || LIST_RENDER_DEFAULT}
                    rules={[{ required: true }]}
                />
                <SelectField
                    name={"rom_id"}
                    label={"Lưu trữ"}
                    options={filterAll?.laptop?.roms || LIST_RENDER_DEFAULT}
                    rules={[{ required: true }]}
                />
                <SelectField
                    name={"gpu_id"}
                    label={"Card đồ họa"}
                    options={filterAll?.laptop?.gpus || LIST_RENDER_DEFAULT}
                    rules={[{ required: true }]}
                />
                <SelectField
                    name={"screen_id"}
                    label={"Kích thướt màn hình"}
                    options={filterAll?.laptop?.screens || LIST_RENDER_DEFAULT}
                    rules={[{ required: true }]}
                />
                <SelectField
                    name={"port_id"}
                    label={"Kết nối chính"}
                    options={filterAll?.laptop?.ports || LIST_RENDER_DEFAULT}
                    rules={[{ required: true }]}
                />
                <SelectField
                    name={"battery_id"}
                    label={"PIN"}
                    options={
                        filterAll?.laptop?.batteries || LIST_RENDER_DEFAULT
                    }
                    rules={[{ required: true }]}
                />
                <SelectField
                    name={"weight_id"}
                    label={"Trọng lượng"}
                    options={filterAll?.laptop?.weights || LIST_RENDER_DEFAULT}
                    rules={[{ required: true }]}
                />
                <SelectField
                    name={"size_id"}
                    label={"Kích thước"}
                    options={filterAll?.laptop?.sizes || LIST_RENDER_DEFAULT}
                    rules={[{ required: true }]}
                />
                <SelectField
                    name={"os_id"}
                    label={"Hệ điều hành"}
                    options={filterAll?.laptop?.os || LIST_RENDER_DEFAULT}
                    rules={[{ required: true }]}
                />
            </Col>
        )
    }
    const renderSpecDrive = () => {
        return (
            <Col span={12}>
                <SelectField
                    name={"drive_type_id"}
                    label={"Kiểu ổ cứng"}
                    options={filterAll?.drive?.types || LIST_RENDER_DEFAULT}
                    onChange={value => {
                        dispatch(
                            setIsSSDForCreateDrive(
                                value % 2 === 0 ? true : false
                            )
                        )
                    }}
                    rules={[{ required: true }]}
                />
                <SelectField
                    name={"capacity_id"}
                    label={"Dung lượng"}
                    options={
                        filterAll?.drive?.capacities || LIST_RENDER_DEFAULT
                    }
                    rules={[{ required: true }]}
                />
                <SelectField
                    name={"connect_id"}
                    label={"Kết nối"}
                    options={
                        filterAll?.drive?.connections || LIST_RENDER_DEFAULT
                    }
                    rules={[{ required: true }]}
                />
                <SelectField
                    name={"dimension_id"}
                    label={"Kích thướt"}
                    options={
                        filterAll?.drive?.dimensions || LIST_RENDER_DEFAULT
                    }
                    rules={[{ required: true }]}
                />
                <SelectField
                    name={"read_id"}
                    label={"Tốc độ đọc"}
                    options={filterAll?.drive?.reads || LIST_RENDER_DEFAULT}
                    rules={[{ required: true }]}
                />
                <SelectField
                    name={"write_id"}
                    label={"Tốc độ ghi"}
                    options={filterAll?.drive?.writes || LIST_RENDER_DEFAULT}
                    rules={[{ required: true }]}
                />
                <SelectField
                    name={"rotation_id"}
                    label={"Tốc độ quay"}
                    options={filterAll?.drive?.rotations || LIST_RENDER_DEFAULT}
                    rules={isSSDForCreateDrive ? [] : [{ required: true }]}
                    disabled={isSSDForCreateDrive}
                />
                <SelectField
                    name={"cache_id"}
                    label={"Bộ nhớm đệm"}
                    options={filterAll?.drive?.caches || LIST_RENDER_DEFAULT}
                    rules={isSSDForCreateDrive ? [] : [{ required: true }]}
                    disabled={isSSDForCreateDrive}
                />
            </Col>
        )
    }

    //------------------------HANDLE RENDER------------------------
    const handleRenderSpec = () => {
        switch (productTypeIdForCreate) {
            case 1:
                return renderSpecLaptop()
            case 2:
                return renderSpecDrive()
            default:
                return <p>Hệ thống đang cập nhật</p>
        }
    }
    const handleRenderBrand = () => {
        switch (productTypeIdForCreate) {
            case 1:
                return filterAll?.laptop?.brands
            case 2:
                return filterAll?.drive?.brands
            default:
                break
        }
    }

    //------------------------UPLOAD IMAGE------------------------------
    const [fileImageUpload, setFileImageUpload] = React.useState(null)
    const [linkImageUpload, setLinkImageUpload] = React.useState(null)
    const [isUploading, setIsUploading] = React.useState(false)

    React.useEffect(() => {
        if (fileImageUpload !== null) {
            handleUploadImageToFirebase()
        }
    }, [fileImageUpload])

    const handleChangeFileImage = e => {
        if (e.target.files[0]) {
            setFileImageUpload(e.target.files[0])
        }
    }

    const handleUploadImageToFirebase = () => {
        alert("Đang tải lên...")
        if (fileImageUpload != null) {
            const storageRef = ref(
                storageFirebase,
                `${FIREBASE_PATH.LAPTOP}/${fileImageUpload.name}`
            )
            const uploadTask = uploadBytesResumable(storageRef, fileImageUpload)
            uploadTask.on(
                "state_changed",
                snapshot => {
                    setIsUploading(true)
                },
                error => alert(error),
                () => {
                    getDownloadURL(uploadTask.snapshot.ref).then(
                        downloadURL => {
                            alert("Link ảnh: " + downloadURL)
                            setLinkImageUpload(downloadURL)
                            setIsUploading(false)
                        }
                    )
                }
            )
        }
    }

    return (
        <div>
            <BreadcrumbField list={["Admin", "Thêm mới"]} />
            <h3 style={{ textAlign: "center", marginBottom: "30px" }}>
                Thêm mới sản phẩm
            </h3>
            <Form
                {...layout}
                name="nest-messages"
                onFinish={onFinish}
                validateMessages={VALIDATE_MESSAGES}
            >
                <Row>
                    <Col span={12}>
                        <SelectField
                            name={"type_id"}
                            label={"Loại sản phẩm"}
                            options={filterAll?.type || LIST_RENDER_DEFAULT}
                            onChange={value => setProductTypeIdForCreate(value)}
                            rules={[{ required: true }]}
                        />
                        <SelectField
                            name={"brand_id"}
                            label={"Hãng sản phẩm"}
                            options={handleRenderBrand() || LIST_RENDER_DEFAULT}
                            rules={[{ required: true }]}
                        />
                        <InputField
                            typeinput={TYPE_CUSTOM_FIELD.INPUT}
                            name={"name"}
                            label={"Tên sản phẩm"}
                            rules={[{ required: true }]}
                        />
                        <InputField
                            typeinput={TYPE_CUSTOM_FIELD.INPUT}
                            name={"price"}
                            label={"Giá sản phẩm"}
                            suffix={"VNĐ"}
                            rules={[{ required: true }]}
                            onChange={e => setPrice(e.target.value)}
                        />
                        <InputField
                            typeinput={TYPE_CUSTOM_FIELD.INPUT_NUMBER}
                            name={"guarantee"}
                            label={"Thời gian bảo hành (tháng)"}
                            rules={[
                                {
                                    required: true,
                                    type: "number",
                                    min: 0,
                                    max: 99
                                }
                            ]}
                        />
                        <InputField
                            typeinput={TYPE_CUSTOM_FIELD.INPUT}
                            name={"uploadImage"}
                            label={"Tải ảnh lên Firebase"}
                            type={"file"}
                            accept={"image/*"}
                            onChange={e => handleChangeFileImage(e)}
                        />
                        <Form.Item name={"linkImage"} label={"Link ảnh [copy]"}>
                            {linkImageUpload || "Chưa tải lên"}
                        </Form.Item>
                        <InputField
                            typeinput={TYPE_CUSTOM_FIELD.INPUT}
                            name={"img1"}
                            label={"Link ảnh 1"}
                            prefix={<LinkOutlined />}
                            rules={[{ required: true }]}
                        />
                        <InputField
                            typeinput={TYPE_CUSTOM_FIELD.INPUT}
                            name={"img2"}
                            label={"Link ảnh 2"}
                            prefix={<LinkOutlined />}
                            rules={[{ required: true }]}
                        />
                        <InputField
                            typeinput={TYPE_CUSTOM_FIELD.INPUT}
                            name={"img3"}
                            label={"Link ảnh 3"}
                            prefix={<LinkOutlined />}
                            rules={[{ required: true }]}
                        />
                        <InputField
                            typeinput={TYPE_CUSTOM_FIELD.BUTTON}
                            name={"btn-description"}
                            label={"Mô tả sản phẩm"}
                            button={
                                <Button
                                    type="primary"
                                    icon={<EditOutlined />}
                                    onClick={() => setOpenDescription(true)}
                                ></Button>
                            }
                        />
                        <InputField
                            typeinput={TYPE_CUSTOM_FIELD.BUTTON}
                            name={"btn-discount"}
                            label={"Khuyến mãi"}
                            button={
                                <Button
                                    type="primary"
                                    icon={<PercentageOutlined />}
                                    style={{
                                        background: "#52c41a",
                                        borderColor: "#52c41a"
                                    }}
                                    onClick={() => setOpenDiscount(true)}
                                ></Button>
                            }
                        />
                    </Col>
                    {/* render col right */}
                    {handleRenderSpec()}
                    <Row
                        style={{
                            width: "100%",
                            display: "flex",
                            justifyContent: "center"
                        }}
                    >
                        <Space size={"small"}>
                            <Button
                                type="primary"
                                htmlType="submit"
                                icon={<SaveOutlined />}
                            >
                                Lưu
                            </Button>
                            <Button
                                text="Hủy"
                                htmlType="cancel"
                                icon={<CloseOutlined />}
                                onClick={() => onCancel()}
                            >
                                Hủy
                            </Button>
                        </Space>
                    </Row>
                </Row>
                <Drawer
                    title="Thêm mô tả sản phẩm"
                    height={"100%"}
                    width={"70%"}
                    closable={true}
                    onClose={() => setOpenDescription(false)}
                    visible={openDescription}
                    footer={
                        <div
                            style={{
                                textAlign: "right"
                            }}
                        >
                            <Button
                                onClick={() => setOpenDescription(false)}
                                type="primary"
                            >
                                Lưu
                            </Button>
                        </div>
                    }
                >
                    <CKEditor
                        editor={ClassicEditor}
                        data="<p>Mô tả...</p>"
                        onChange={(event, editor) => {
                            setDescription(editor.getData())
                        }}
                    />
                </Drawer>
                <Drawer
                    title="Thêm khuyến mãi"
                    width={"40%"}
                    closable={true}
                    onClose={() => setOpenDiscount(false)}
                    visible={openDiscount}
                    footer={
                        <div
                            style={{
                                textAlign: "right"
                            }}
                        >
                            <Button
                                onClick={() => setOpenDiscount(false)}
                                type="primary"
                            >
                                Lưu
                            </Button>
                        </div>
                    }
                >
                    <>
                        <Statistic
                            title="Giá gốc"
                            value={price && changePriceToVND(price)}
                        />
                        <Statistic
                            title="Giá khuyến mãi"
                            value={
                                discountPrice && changePriceToVND(discountPrice)
                            }
                        />
                        <h4>Phần trăm khuyển mãi</h4>
                        <Form.Item
                            name={"percent"}
                            initialValue={discountPercent && discountPercent}
                        >
                            <InputNumber
                                size="large"
                                min={0}
                                max={99}
                                onChange={e => {
                                    setDiscountPercent(e)
                                    setDiscountPrice(
                                        price - (discountPrice * e) / 100
                                    )
                                }}
                            />
                        </Form.Item>
                        <h4>Thời gian khuyến mãi</h4>
                        <Form.Item name={"discountTime"}>
                            <RangePicker
                                showTime={{ format: "HH:mm" }}
                                format="YYYY-MM-DD HH:mm"
                                onChange={(value, dateString) =>
                                    setDiscountTime(dateString)
                                }
                            />
                        </Form.Item>
                    </>
                </Drawer>
            </Form>
        </div>
    )
}

export default AddProductPage
