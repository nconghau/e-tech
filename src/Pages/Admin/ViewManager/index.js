import { FileSearchOutlined, ReloadOutlined } from "@ant-design/icons"
import { Button, Modal, notification, Table, Tag } from "antd"
import Search from "antd/lib/input/Search"
import { Content } from "antd/lib/layout/layout"
import BreadcrumbField from "Components/Admin/CustomFields/BreadcrumbField"
import React, { useEffect, useState } from "react"
import { useDispatch, useSelector } from "react-redux"
import { handleSearchByName } from "Redux/Admin/Bill/BillAdmin.reducer"
import {
    changeStatusBillApi,
    getBillListApi
} from "Redux/Admin/Bill/BillAdmin.thunk"
import { changePriceToVND } from "Utils/Converter"
const STATUS = [
    {
        text: "Đang chờ xác nhận",
        value: "AWAIT_FOR_CONFIRMATION"
    },
    {
        text: "Đang giao",
        value: "ON_GOING"
    },
    {
        text: "Đã giao",
        value: "DELIVERED"
    },
    {
        text: "Hủy đơn",
        value: "CANCELLED"
    }
]
const ViewManager = () => {
    const [isModalVisible, setIsModalVisible] = useState(false)
    const [billDetail, setBillDetail] = useState(null)

    const handleCancel = () => {
        setIsModalVisible(false)
    }
    const dispatch = useDispatch()

    const billsFilter = useSelector(state => state.BillReducer.billsFilter)
    const curr_page = useSelector(state => state.BillReducer.curr_page)
    const max_page = useSelector(state => state.BillReducer.max_page)

    useEffect(() => {
        dispatch(getBillListApi(1))
    }, [])

    // ---------------------EVEN---------------------

    const handleGetBills = (page, pageSize) => {
        dispatch(getBillListApi(page))
    }

    const openNotify = (type, title, message) => {
        notification[type]({
            message: title,
            description: message
        })
    }

    const showBillDetail = record => {
        setIsModalVisible(true)
        setBillDetail(record)
    }

    const changeBillStatus = (billId, status) => {
        dispatch(changeStatusBillApi(billId, status)).then(notify =>
            openNotify(notify.type, notify.title, notify.message)
        )
        setIsModalVisible(false)
        dispatch(getBillListApi(1))
    }

    const changeStatusVN = status => {
        switch (status) {
            case "AWAIT_FOR_CONFIRMATION":
                return "Đang chờ xác nhận"
            case "ON_GOING":
                return "Đang giao"
            case "DELIVERED":
                return "Đã giao"
            case "CANCELLED":
                return "Hủy đơn"
            default:
                return "-----"
        }
    }

    // ---------------------COL---------------------

    const columns = [
        {
            key: "billId",
            title: "Mã HĐ",
            dataIndex: "billId",
            width: 120,
            sorter: (a, b) => a.billId - b.billId,
            defaultSortOrder: "descend",
            fixed: "left"
        },
        {
            key: "userId",
            title: "Mã KH",
            dataIndex: "userId",
            width: 120,
            sorter: (a, b) => a.userId - b.userId,
            fixed: "left"
        },
        {
            key: "timeBuy",
            title: "Thời gian",
            dataIndex: "timeBuy",
            width: 200,
            render: (value, record) => (
                <p>{new Date(record?.bill?.timeBuy).toLocaleString("en-GB")}</p>
            ),
            sorter: (a, b) => a.timeBuy - b.timeBuy
        },
        {
            key: "name",
            title: "Tên khách hàng",
            dataIndex: "name",
            sorter: (a, b) => a.name.localeCompare(b.name)
        },
        {
            key: "email",
            title: "Email",
            dataIndex: "email"
        },
        {
            key: "address",
            title: "Địa chỉ",
            dataIndex: "address"
        },
        {
            key: "note",
            title: "Ghi chú",
            dataIndex: "note"
        },
        {
            key: "phone",
            title: "Số điện thoại",
            dataIndex: "phone",
            width: 150
        },
        {
            key: "status",
            title: "Trạng thái đơn hàng",
            dataIndex: "status",
            filters: STATUS,
            onFilter: (value, record) => record.status.includes(value),
            defaultFilteredValue: ["AWAIT_FOR_CONFIRMATION"],
            render: status => (
                <Tag
                    color={
                        status === "ON_GOING"
                            ? "yellow"
                            : status === "AWAIT_FOR_CONFIRMATION"
                            ? "blue"
                            : status === "DELIVERED"
                            ? "green"
                            : "red"
                    }
                    key={status}
                >
                    {changeStatusVN(status)}
                </Tag>
            )
            // width: 220,
            // fixed: "left"
        },
        {
            key: "bill",
            title: "Chi tiết",
            dataIndex: "bill",
            width: 100,
            align: "center",
            render: (value, record) => (
                <Button
                    type="primary"
                    icon={<FileSearchOutlined />}
                    onClick={() => showBillDetail(record)}
                ></Button>
            )
        }
    ]

    const columnsProduct = [
        {
            title: "Mã sản phẩm",
            dataIndex: "id",
            key: "bii_id",
            width: 160,
            align: "center"
        },
        {
            title: "Tên sản phẩm",
            dataIndex: "name",
            key: "bill_name",
            width: 250,
            align: "center"
        },
        {
            title: "Ảnh",
            dataIndex: "image",
            key: "bill_image",
            render: image => <img alt="" src={image} width="100%"></img>,
            width: 160,
            align: "center"
        },
        {
            title: "Giá",
            dataIndex: "price",
            key: "bill_price",
            render: price => changePriceToVND(price),
            align: "center"
        },
        {
            title: "Số lượng",
            dataIndex: "qty",
            key: "bill_qty",
            align: "center",
            width: 120
        }
    ]

    return (
        <>
            <BreadcrumbField list={["Admin", "Quản lý đơn hàng"]} />
            <br />
            <Content
                style={{
                    overflow: "initial"
                }}
            >
                <Button
                    type="primary"
                    style={{ marginBottom: 5, width: 80 }}
                    icon={<ReloadOutlined />}
                    onClick={() => dispatch(getBillListApi(1))}
                />
                <Search
                    placeholder="Tìm kiếm tên khách hàng"
                    style={{ width: 200, marginLeft: 5 }}
                    onChange={e => {
                        dispatch(handleSearchByName(e.target.value))
                    }}
                />
                <br />
                <Table
                    className="components-table-demo-nested"
                    columns={columns}
                    dataSource={billsFilter}
                    pagination={{
                        defaultPageSize: 15,
                        current: curr_page,
                        total: max_page * 15,
                        onChange: (page, pageSize) => {
                            handleGetBills(page, pageSize)
                        },
                        pageSizeOptions: []
                    }}
                    scroll={{ y: 650 }}
                />
                <Modal
                    title={"Chi tiết hóa đơn"}
                    visible={isModalVisible}
                    // onOk={handleOk}
                    onCancel={handleCancel}
                    width={1000}
                    footer={[
                        <Button
                            type="primary"
                            onClick={() =>
                                changeBillStatus(
                                    billDetail?.bill?.billId,
                                    "AWAIT_FOR_CONFIRMATION"
                                )
                            }
                        >
                            Chờ xác nhận
                        </Button>,
                        <Button
                            type="primary"
                            style={{
                                background: "#fadb14",
                                borderColor: "#fadb14"
                            }}
                            onClick={() =>
                                changeBillStatus(
                                    billDetail?.bill?.billId,
                                    "ON_GOING"
                                )
                            }
                        >
                            Giao hàng
                        </Button>,
                        <Button
                            type="primary"
                            style={{
                                background: "#73d13d",
                                borderColor: "#73d13d"
                            }}
                            onClick={() =>
                                changeBillStatus(
                                    billDetail?.bill?.billId,
                                    "DELIVERED"
                                )
                            }
                        >
                            Đã giao
                        </Button>,
                        <Button
                            danger
                            type="primary"
                            onClick={() =>
                                changeBillStatus(
                                    billDetail?.bill?.billId,
                                    "CANCELLED"
                                )
                            }
                        >
                            Hủy đơn
                        </Button>
                    ]}
                >
                    {
                        <Table
                            columns={columnsProduct}
                            pagination={false}
                            dataSource={billDetail?.bill?.products}
                            title={() => (
                                <div>
                                    <b>{`Mã hóa đơn: ${billDetail?.bill?.billId}`}</b>
                                    <br />
                                    <b>{`Thời gian mua: ${
                                        billDetail?.bill &&
                                        new Date(
                                            billDetail?.bill?.timeBuy
                                        ).toLocaleString("en-GB")
                                    }`}</b>
                                    <br />
                                    <b>{`Khách hàng: ${billDetail?.name}`}</b>
                                    <br />
                                    <b>{`Trạng thái đơn hàng: ${changeStatusVN(
                                        billDetail?.status
                                    )}`}</b>
                                    <br />
                                    <b>{`Ghi chú: ${billDetail?.note}`}</b>
                                </div>
                            )}
                            footer={() => (
                                <b
                                    style={{
                                        display: "flex",
                                        justifyContent: "right"
                                    }}
                                >{`Tổng đơn: ${
                                    billDetail?.bill &&
                                    changePriceToVND(
                                        billDetail?.bill?.totalPrice
                                    )
                                }`}</b>
                            )}
                        />
                    }
                </Modal>
            </Content>
        </>
    )
}

export default ViewManager
