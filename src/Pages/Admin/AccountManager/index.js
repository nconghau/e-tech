import { ReloadOutlined } from "@ant-design/icons"
import { Button, Table, Tag } from "antd"
import Search from "antd/lib/input/Search"
import { Content } from "antd/lib/layout/layout"
import BreadcrumbField from "Components/Admin/CustomFields/BreadcrumbField"
import React, { useEffect } from "react"
import { useDispatch, useSelector } from "react-redux"
import { getBillListApi } from "Redux/Admin/Bill/BillAdmin.thunk"
import { handleSearchByName } from "Redux/Admin/User/UserAdmin.reducer"
import { getUsers } from "Redux/Admin/User/UserAdmin.thunk"

const STATUS = [
    {
        text: "Admin",
        value: true
    },
    {
        text: "Người dùng",
        value: false
    }
]

const AccountManager = () => {
    const dispatch = useDispatch()
    const usersFilter = useSelector(state => state.UserAdminReducer.usersFilter)
    const curr_page = useSelector(state => state.UserAdminReducer.curr_page)
    const max_page = useSelector(state => state.UserAdminReducer.max_page)

    useEffect(() => {
        dispatch(getUsers(1))
    }, [])

    const handleGetUsers = (page, pageSize) => {
        dispatch(getUsers(page))
    }

    // ---------------------COL---------------------
    const columns = [
        {
            key: "id",
            title: "Mã KH",
            dataIndex: "id",
            width: 120,
            sorter: (a, b) => a.userId - b.userId,
            fixed: "left"
        },
        {
            key: "name",
            title: "Tên khách hàng",
            dataIndex: "name",
            sorter: (a, b) => a.name.localeCompare(b.name)
        },
        {
            key: "gender",
            title: "Giới tính",
            dataIndex: "gender",
            render: is_admin =>
                is_admin === null ? "--" : is_admin ? "Nam" : "Nữ",
            width: 150
        },
        {
            key: "email",
            title: "Email",
            dataIndex: "email"
        },
        {
            key: "address",
            title: "Địa chỉ",
            dataIndex: "address"
        },
        {
            key: "phone",
            title: "Số điện thoại",
            dataIndex: "phone",
            width: 150
        },
        {
            key: "order",
            title: "Tổng số đơn hàng",
            dataIndex: "order",
            sorter: (a, b) => a - b,
            width: 150
        },
        {
            key: "is_admin",
            title: "Quyền",
            dataIndex: "is_admin",
            filters: STATUS,
            onFilter: (value, record) => record?.is_admin === value,
            defaultFilteredValue: [false],
            render: is_admin => (
                <Tag color={is_admin === true ? "red" : "green"}>
                    {is_admin ? "Admin" : "Người dùng"}
                </Tag>
            ),
            width: 220
        }
    ]

    return (
        <>
            <BreadcrumbField list={["Admin", "Quản lý tài khoản"]} />
            <br />
            <Content
                style={{
                    overflow: "initial"
                }}
            >
                <Button
                    type="primary"
                    style={{ marginBottom: 5, width: 80 }}
                    icon={<ReloadOutlined />}
                    onClick={() => dispatch(getBillListApi())}
                />
                <Search
                    placeholder="Tìm kiếm tên tài khoản"
                    style={{ width: 200, marginLeft: 5 }}
                    onChange={e => {
                        dispatch(handleSearchByName(e.target.value))
                    }}
                />
                <br />
                <Table
                    className="components-table-demo-nested"
                    columns={columns}
                    dataSource={usersFilter}
                    pagination={{
                        defaultPageSize: 15,
                        current: curr_page,
                        total: max_page * 15,
                        onChange: (page, pageSize) => {
                            handleGetUsers(page, pageSize)
                        },
                        pageSizeOptions: []
                    }}
                    scroll={{ y: 650 }}
                />
            </Content>
        </>
    )
}

export default AccountManager
