import { UserOutlined } from "@ant-design/icons"
import { Button, Form, notification } from "antd"
import InputField from "Components/Admin/CustomFields/InputField"
import { TYPE_CUSTOM_FIELD } from "Constants/Data"
import { PATH } from "Constants/Path"
import React from "react"
import { useDispatch } from "react-redux"
import { useHistory } from "react-router-dom"
import { Link } from "react-router-dom"
import { forgotPassword } from "Redux/User/User.thunk"
import "./ForgotPasswordPage.css"
const ForgotPasswordPage = () => {
    const [form] = Form.useForm()
    const dispatch = useDispatch()
    const history = useHistory()
    const redirectHomePage = () => history.push(PATH.HOME)

    const onFinish = async values => {
        const res = await dispatch(forgotPassword(values.email))
        if (res?.success) {
            notification.success({
                message: "Thông báo",
                description: "Mời bạn kiểm tra email"
            })
            redirectHomePage()
        } else {
            form.setFields([
                {
                    name: "email",
                    errors: ["Email chưa đúng!"]
                }
            ])
        }
    }

    return (
        <div className="form-container">
            <div className="form-login">
                <Form form={form} name="normal_login" onFinish={onFinish}>
                    <Form.Item>
                        <h2 className="form-title">Quên Mật Khẩu</h2>
                    </Form.Item>

                    <InputField
                        typeinput={TYPE_CUSTOM_FIELD.INPUT}
                        name={"email"}
                        prefix={
                            <UserOutlined className="site-form-item-icon" />
                        }
                        placeholder="Email"
                        rules={[
                            {
                                required: true,
                                message: "Mời bạn nhập email"
                            },
                            {
                                type: "email",
                                message: "Chưa là email"
                            }
                        ]}
                    />
                    <Form.Item className="form-end">
                        <div className="form-end-wrapper-button">
                            <Button
                                type="primary"
                                htmlType="submit"
                                className="form-end-button"
                            >
                                Xác Nhận Qua Email
                            </Button>
                        </div>
                        <p className="form-end-signup">
                            Bạn chưa có tài khoản?
                            <Link to={PATH.SIGNUP}>&emsp;Đăng Ký</Link>
                        </p>
                        <p className="form-end-signup">
                            Bạn có tài khoản?
                            <Link to={PATH.LOGIN}>&emsp;Đăng Nhập</Link>
                        </p>
                    </Form.Item>
                </Form>
            </div>
        </div>
    )
}

export default ForgotPasswordPage
