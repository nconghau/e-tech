import { LockOutlined, UserOutlined } from "@ant-design/icons"
import { Button, Checkbox, Form } from "antd"
import InputField from "Components/Admin/CustomFields/InputField"
import { TYPE_CUSTOM_FIELD } from "Constants/Data"
import { PATH } from "Constants/Path"
import React from "react"
import { useDispatch } from "react-redux"
import { Link, useHistory } from "react-router-dom"
import { login } from "Redux/User/User.thunk"
import Cookies from "js-cookie"
import "./LoginPage.css"
const LoginPage = () => {
    const [account, setAccount] = React.useState({
        email: Cookies.get("email"),
        password: Cookies.get("password")
    })
    const [isRemember, setIsRemember] = React.useState(false)
    const [form] = Form.useForm()
    const history = useHistory()
    // localStorage.clear()
    const redirectHomePage = () => history.push(PATH.HOME)

    const dispatch = useDispatch()

    const onFinish = async values => {
        const isLogin = await dispatch(
            login({
                email: values.email,
                password: values.password,
                isRemember
            })
        )

        if (isLogin) {
            redirectHomePage()
        } else {
            form.setFields([
                {
                    name: "password",
                    errors: ["Email hoặc mật khẩu chưa đúng!"]
                }
            ])
        }
    }

    return (
        <div className="form-container">
            <div className="form-login">
                <Form
                    form={form}
                    name="normal_login"
                    initialValues={account}
                    onFinish={onFinish}
                >
                    <Form.Item>
                        <h2 className="form-title">Đăng Nhập</h2>
                    </Form.Item>

                    <InputField
                        typeinput={TYPE_CUSTOM_FIELD.INPUT}
                        name={"email"}
                        prefix={
                            <UserOutlined className="site-form-item-icon" />
                        }
                        placeholder="Email"
                        rules={[
                            {
                                required: true,
                                message: "Mời bạn nhập email"
                            },
                            {
                                type: "email",
                                message: "Chưa là email"
                            }
                        ]}
                    />
                    <InputField
                        typeinput={TYPE_CUSTOM_FIELD.PASSWORD}
                        name={["password"]}
                        prefix={
                            <LockOutlined className="site-form-item-icon" />
                        }
                        placeholder="Mật Khẩu"
                        rules={[
                            {
                                required: true,
                                message: "Mời Nhập Mật Khẩu!"
                            }
                        ]}
                    />

                    <div className="form-password">
                        <Form.Item
                            name="remember"
                            valuePropName="checked"
                            noStyle
                        >
                            <Checkbox
                                checked={isRemember}
                                onChange={() => setIsRemember(!isRemember)}
                            >
                                Lưu mật khẩu
                            </Checkbox>
                        </Form.Item>
                        <Link
                            className="login-form-forgot"
                            to={PATH.FORGOT_PASSWORD}
                        >
                            &emsp;Quên mật khẩu?
                        </Link>
                    </div>

                    <Form.Item className="form-end">
                        <div className="form-end-wrapper-button">
                            <Button
                                type="primary"
                                htmlType="submit"
                                className="form-end-button"
                            >
                                Đăng Nhập
                            </Button>
                        </div>
                        <p className="form-end-signup">
                            Bạn chưa có tài khoản?
                            <Link to={PATH.SIGNUP}>&emsp;Đăng Ký</Link>
                        </p>
                    </Form.Item>
                </Form>
            </div>
        </div>
    )
}

export default LoginPage
