import { Button, Form, notification, Radio } from "antd"
import InputField from "Components/Admin/CustomFields/InputField"
import { TYPE_CUSTOM_FIELD } from "Constants/Data"
import { PATH } from "Constants/Path"
import React, { useState } from "react"
import { useDispatch } from "react-redux"
import { useHistory } from "react-router"
import { Link } from "react-router-dom"
import { login, signup } from "Redux/User/User.thunk"
import "./SignUpPage.css"

const FormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0
        },
        sm: {
            span: 16,
            offset: 8
        }
    }
}
const formItemLayout = {
    labelCol: {
        xs: {
            span: 24
        },
        sm: {
            span: 8
        }
    },
    wrapperCol: {
        xs: {
            span: 24
        },
        sm: {
            span: 16
        }
    }
}

const SignupPage = () => {
    const [form] = Form.useForm()
    const onReset = () => {
        form.resetFields()
    }
    const history = useHistory()
    const redirectHomePage = () => history.push(PATH.HOME)
    const dispatch = useDispatch()
    const [gender, setGender] = useState(false)
    const onFinish = async values => {
        const body = {
            name: values.name,
            email: values.email,
            password: values.password,
            phone: values.phone,
            address: values.address,
            gender: gender
        }
        const res = await dispatch(signup(body))
        if (res?.data?.success === true) {
            const isLogin = dispatch(
                login({
                    email: values.email,
                    password: values.password
                })
            )
            if (isLogin) {
                notification.success({
                    message: "Thông báo",
                    description: "Đăng ký thành công!"
                })
                redirectHomePage()
            }
        } else {
            notification.error({
                message: "Thông báo",
                description: "Đăng ký thất bại!"
            })
            form.setFields([
                {
                    name: "email",
                    errors: ["Đã tồn tại email này!"]
                }
            ])
        }
    }

    return (
        <div className="form-container">
            <div className="form-signup">
                <Form
                    {...formItemLayout}
                    form={form}
                    name="register"
                    onFinish={onFinish}
                    scrollToFirstError
                >
                    <Form.Item {...FormItemLayout}>
                        <h2 className="form-title">Đăng Ký</h2>
                    </Form.Item>
                    <InputField
                        typeinput={TYPE_CUSTOM_FIELD.INPUT}
                        name={"email"}
                        label={"Email"}
                        rules={[
                            {
                                required: true,
                                message: "Mời bạn nhập email"
                            },
                            {
                                type: "email",
                                message: "Chưa là email"
                            }
                        ]}
                    />
                    <InputField
                        typeinput={TYPE_CUSTOM_FIELD.PASSWORD}
                        name={["password"]}
                        label="Mật Khẩu"
                        hasFeedback={true}
                        rules={[
                            {
                                required: true,
                                message: "Mời bạn nhập mật khẩu"
                            },
                            {
                                message: "Mật khẩu phải hơn 8 ký tự",
                                min: 8
                            }
                        ]}
                    />
                    <InputField
                        typeinput={TYPE_CUSTOM_FIELD.PASSWORD}
                        name="confirm"
                        label="Xác Nhận Mật Khẩu"
                        dependencies={["password"]}
                        hasFeedback={true}
                        rules={[
                            {
                                required: true,
                                message: "Mời bạn xác nhận mật khẩu"
                            },
                            ({ getFieldValue }) => ({
                                validator(_, value) {
                                    if (
                                        value &&
                                        getFieldValue("password") === value
                                    ) {
                                        return Promise.resolve()
                                    }

                                    return Promise.reject(
                                        new Error("Mật khẩu không khớp")
                                    )
                                }
                            })
                        ]}
                    />
                    <InputField
                        typeinput={TYPE_CUSTOM_FIELD.INPUT}
                        name={"name"}
                        label={"Tên Khách Hàng"}
                        rules={[
                            {
                                required: true,
                                message: "Mời bạn nhập tên"
                            }
                        ]}
                    />
                    <InputField
                        typeinput={TYPE_CUSTOM_FIELD.INPUT}
                        name={"address"}
                        label={"Địa Chỉ"}
                        rules={[
                            {
                                required: true,
                                message: "Mời bạn nhập địa chỉ"
                            }
                        ]}
                    />
                    <InputField
                        typeinput={TYPE_CUSTOM_FIELD.INPUT}
                        name={"phone"}
                        label={"Số Điện Thoại"}
                        rules={[
                            {
                                required: true,
                                message: "Mời bạn nhập số điện thoại"
                            },
                            {
                                max: 10,
                                message: "Số điện thoại không lớn hơn 10"
                            },
                            {
                                min: 10,
                                message: "Số điện thoại không nhỏ hơn 10"
                            },
                            {
                                pattern: RegExp("^[0-9]+$"),
                                message: "Chưa là SĐT"
                            }
                        ]}
                    />
                    <Form.Item name="gender" label="Giới tính">
                        <Radio.Group
                            onChange={() => setGender(!gender)}
                            value={gender}
                        >
                            <Radio value={true}>Nam</Radio>
                            <Radio value={false}>Nữ</Radio>
                        </Radio.Group>
                    </Form.Item>
                    <InputField
                        typeinput={TYPE_CUSTOM_FIELD.CHECKBOX}
                        name="agreement"
                        labelCheckbox={"Tôi đồng ý chính sách của ETech."}
                        valuePropName="checked"
                        rules={[
                            {
                                validator: (_, value) =>
                                    value
                                        ? Promise.resolve()
                                        : Promise.reject(
                                              new Error("Bạn chưa ấn đồng ý")
                                          )
                            }
                        ]}
                        {...FormItemLayout}
                    />

                    <Form.Item {...FormItemLayout} className="form-end">
                        <Button
                            htmlType="submit"
                            type="primary"
                            className="form-end-button-signup"
                        >
                            Đăng Ký
                        </Button>

                        <Button
                            htmlType="button"
                            onClick={onReset}
                            className="form-end-button-reset"
                        >
                            Tạo Lại
                        </Button>
                        <p className="form-end-login">
                            Bạn có tài khoản?
                            <Link to={PATH.LOGIN}>Đăng Nhập</Link>
                        </p>
                    </Form.Item>
                </Form>
            </div>
        </div>
    )
}

export default SignupPage
