import { Button, Form, notification, Radio } from "antd"
import InputField from "Components/Admin/CustomFields/InputField"
import { TYPE_CUSTOM_FIELD } from "Constants/Data"
import { PATH } from "Constants/Path"
import React, { useEffect, useState } from "react"
import { useDispatch, useSelector } from "react-redux"
import { useHistory } from "react-router"
import { Link } from "react-router-dom"
import { getUserCookie, updateInfo } from "Redux/User/User.thunk"
import "../SignUp/SignUpPage.css"

const FormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0
        },
        sm: {
            span: 16,
            offset: 8
        }
    }
}
const formItemLayout = {
    labelCol: {
        xs: {
            span: 24
        },
        sm: {
            span: 8
        }
    },
    wrapperCol: {
        xs: {
            span: 24
        },
        sm: {
            span: 16
        }
    }
}

const UpdateInfoPage = () => {
    const [form] = Form.useForm()
    const onReset = () => {
        form.resetFields()
    }
    const history = useHistory()
    const redirectLoginPage = () => history.push(PATH.LOGIN)
    const dispatch = useDispatch()
    const account = useSelector(state => state.UserReducer.user)
    const [gender, setGender] = useState(account?.gender === 1 ? true : false)
    useEffect(() => {
        dispatch(getUserCookie())
    }, [])
    useEffect(() => {
        form.setFieldsValue({
            name: account?.name,
            email: account?.email,
            phone: account?.phone,
            address: account?.address,
            gender: account?.gender === 1 ? true : false
        })
    }, [account])
    const onFinish = async values => {
        const body = {
            name: values.name,
            email: values.email,
            password: values.password,
            phone: values.phone,
            address: values.address,
            gender: gender
        }
        const res = await dispatch(updateInfo(body))
        if (res?.data?.result) {
            notification.success({
                message: "Thông báo",
                description: "Cập nhật thành công, vui lòng đăng nhập lại!"
            })
            redirectLoginPage()
        } else {
            notification.error({
                message: "Thông báo",
                description: "Cập nhật thất bại, vui lòng kiểm tra lại!"
            })
        }
    }

    return (
        <div className="form-container">
            <div className="form-signup">
                <Form
                    {...formItemLayout}
                    form={form}
                    name="register"
                    onFinish={onFinish}
                    scrollToFirstError
                >
                    <Form.Item {...FormItemLayout}>
                        <h2 className="form-title">Cập Nhật Tài Khoản</h2>
                    </Form.Item>
                    <InputField
                        typeinput={TYPE_CUSTOM_FIELD.INPUT}
                        name={"email"}
                        label={"Email"}
                        rules={[
                            {
                                required: true,
                                message: "Mời bạn nhập email"
                            },
                            {
                                type: "email",
                                message: "Chưa là email"
                            }
                        ]}
                    />
                    <InputField
                        typeinput={TYPE_CUSTOM_FIELD.INPUT}
                        name={"name"}
                        label={"Tên Khách Hàng"}
                        rules={[
                            {
                                required: true,
                                message: "Mời bạn nhập tên"
                            }
                        ]}
                    />
                    <InputField
                        typeinput={TYPE_CUSTOM_FIELD.INPUT}
                        name={"address"}
                        label={"Địa Chỉ"}
                        rules={[
                            {
                                required: true,
                                message: "Mời bạn nhập địa chỉ"
                            }
                        ]}
                    />
                    <InputField
                        typeinput={TYPE_CUSTOM_FIELD.INPUT}
                        name={"phone"}
                        label={"Số Điện Thoại"}
                        rules={[
                            {
                                required: true,
                                message: "Mời bạn nhập số điện thoại"
                            },
                            {
                                max: 10,
                                message: "Số điện thoại không lớn hơn 10"
                            },
                            {
                                min: 10,
                                message: "Số điện thoại không nhỏ hơn 10"
                            },
                            {
                                pattern: RegExp("^[0-9]+$"),
                                message: "Chưa là SĐT"
                            }
                        ]}
                    />
                    <Form.Item name="gender" label="Giới tính">
                        <Radio.Group
                            onChange={() => setGender(!gender)}
                            value={gender}
                        >
                            <Radio value={true}>Nam</Radio>
                            <Radio value={false}>Nữ</Radio>
                        </Radio.Group>
                    </Form.Item>
                    <InputField
                        typeinput={TYPE_CUSTOM_FIELD.CHECKBOX}
                        name="agreement"
                        labelCheckbox={"Tôi đồng ý chính sách của ETech."}
                        valuePropName="checked"
                        rules={[
                            {
                                validator: (_, value) =>
                                    value
                                        ? Promise.resolve()
                                        : Promise.reject(
                                              new Error("Bạn chưa ấn đồng ý")
                                          )
                            }
                        ]}
                        {...FormItemLayout}
                    />

                    <Form.Item {...FormItemLayout} className="form-end">
                        <Button
                            htmlType="submit"
                            type="primary"
                            className="form-end-button-signup"
                        >
                            Cập Nhật
                        </Button>

                        <Button
                            htmlType="button"
                            onClick={onReset}
                            className="form-end-button-reset"
                        >
                            Tạo Lại
                        </Button>
                        <p className="form-end-login">
                            Bạn có tài khoản?
                            <Link to={PATH.LOGIN}>Đăng Nhập</Link>
                        </p>
                    </Form.Item>
                </Form>
            </div>
        </div>
    )
}

export default UpdateInfoPage
