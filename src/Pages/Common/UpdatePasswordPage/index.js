import { Button, Form, notification } from "antd"
import InputField from "Components/Admin/CustomFields/InputField"
import { TYPE_CUSTOM_FIELD } from "Constants/Data"
import { PATH } from "Constants/Path"
import React from "react"
import { useDispatch } from "react-redux"
import { useHistory } from "react-router"
import { useLocation } from "react-router-dom"
import { Link, useParams } from "react-router-dom"
import { login, resetPassword } from "Redux/User/User.thunk"
import "./UpdatePasswordPage.css"

const UpdatePasswordPage = () => {
    const { type } = useParams()
    const location = useLocation()
    function useQuery() {
        return new URLSearchParams(location.search)
    }
    let params = useQuery()
    const TYPE_PAGE = {
        UPDATE_PASSWORD: "cap-nhat",
        CHANGE_PASSWORD: "doi-mat-khau"
    }
    const isUpdatePasswordPage = type === TYPE_PAGE.UPDATE_PASSWORD
    const [form] = Form.useForm()
    const history = useHistory()
    const redirectHomePage = () => history.push(PATH.HOME)
    const dispatch = useDispatch()

    const onFinish = async values => {
        if (isUpdatePasswordPage) {
            const body = {
                email: params.getAll("email").toString(),
                password: values.password,
                token: params.getAll("token").toLocaleString()
            }
            const res = await dispatch(resetPassword(body))
            if (res?.data?.result) {
                const isLogin = dispatch(
                    login({
                        email: params.getAll("email").toString(),
                        password: values.password
                    })
                )
                if (isLogin) {
                    notification.success({
                        message: "Thông báo",
                        description: "Cập nhật mật khẩu thành công!"
                    })
                    redirectHomePage()
                }
            } else {
                notification.error({
                    message: "Thông báo",
                    description:
                        "Cập nhật mật khẩu thất bại, vui lòng kiểm tra lại!"
                })
            }
        }
    }

    return (
        <div className="form-container">
            <div className="form-signup">
                <Form
                    form={form}
                    name="register"
                    onFinish={onFinish}
                    scrollToFirstError
                >
                    <Form.Item>
                        <h2 className="form-title">
                            {isUpdatePasswordPage
                                ? "Cập Nhật Mật Khẩu"
                                : "Đổi Mật Khẩu"}
                        </h2>
                    </Form.Item>
                    {!isUpdatePasswordPage && (
                        <>
                            <InputField
                                typeinput={TYPE_CUSTOM_FIELD.INPUT}
                                name={"email"}
                                placeholder={"Email"}
                            />
                            <InputField
                                typeinput={TYPE_CUSTOM_FIELD.INPUT}
                                name={"passwordOld"}
                                placeholder={"Mật Khẩu Cũ"}
                                rules={[
                                    {
                                        required: true,
                                        message: "Mời bạn nhập"
                                    }
                                ]}
                            />
                        </>
                    )}

                    <InputField
                        typeinput={TYPE_CUSTOM_FIELD.PASSWORD}
                        name={["password"]}
                        placeholder="Mật Khẩu"
                        hasFeedback={true}
                        rules={[
                            {
                                required: true,
                                message: "Mời bạn nhập mật khẩu"
                            },
                            {
                                message: "Mật khẩu phải hơn 8 ký tự",
                                min: 8
                            }
                        ]}
                    />
                    <InputField
                        typeinput={TYPE_CUSTOM_FIELD.PASSWORD}
                        name="confirm"
                        placeholder="Xác Nhận Mật Khẩu"
                        dependencies={["password"]}
                        hasFeedback={true}
                        rules={[
                            {
                                required: true,
                                message: "Mời bạn xác nhận mật khẩu"
                            },
                            ({ getFieldValue }) => ({
                                validator(_, value) {
                                    if (
                                        value &&
                                        getFieldValue("password") === value
                                    ) {
                                        return Promise.resolve()
                                    }

                                    return Promise.reject(
                                        new Error("Mật khẩu không khớp")
                                    )
                                }
                            })
                        ]}
                    />
                    <Form.Item className="form-end">
                        <div className="form-end-wrapper-button">
                            <Button
                                type="primary"
                                htmlType="submit"
                                className="form-end-button"
                            >
                                {isUpdatePasswordPage
                                    ? "Cập Nhật"
                                    : "Đổi Mật Khẩu"}
                            </Button>
                        </div>
                        <p className="form-end-signup">
                            Bạn chưa có tài khoản?
                            <Link to={PATH.SIGNUP}>&emsp;Đăng Ký</Link>
                        </p>
                        <p className="form-end-signup">
                            Bạn có tài khoản?
                            <Link to={PATH.LOGIN}>&emsp;Đăng Nhập</Link>
                        </p>
                    </Form.Item>
                </Form>
            </div>
        </div>
    )
}

export default UpdatePasswordPage
